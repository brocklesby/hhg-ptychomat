function  [diffraction_pattern, mask_oversat, noise_pattern,probe_positions, probe,  scale, lambda, setup] ...
        = prepare_data(data_path, filename, user_scale_size, lambda, extra_binning, deconv_size)
    
% this needs the get_probe function, which at present is in +core
 
if nargin == 0
    user_scale_size = 1;   
end
%addpath('../utils') % wsb - don't think this is needed
%addpath('../')

% keyboard
try
jDesktop = com.mathworks.mde.desk.MLDesktop.getInstance;
jDesktop.getMainFrame.setTitle(filename);
end

try
   % xxx  %what does this do?
    
    %     keyboard
tic
    disp('loading cache')
    [pathstr,name,ext] = fileparts(filename) ;
    %load(['data/fast_cache/', name, '-cache'])
    load(['data/fast_cache/', name, '-cache'])
    disp('cache loaded')
toc


catch
    
        disp('loading full data ... ')
         data  = load([data_path, '/',filename]);
        disp('loaded')
        setup = data.setup;
        time = data.time;
        
        
%         keyboard
        
        Npos = length( data.scan );
        Npix = size( data.scan{1}.data);

%         for  i = 1:Npos
%             data.scan{i}.data = replace_nans(data.scan{i}.data, data.scan{i}.mask );
%         end
        
       
        total_sum = zeros(Npix);
        for  i = 1:Npos
            d =  data.scan{i}.data;
            d(isnan(d)) = 0;
            total_sum = total_sum+ d.^2./ Npos.^2 ;
        end
        [~, xshift, yshift] = center_pattern(total_sum, 'hybrid');
        
        disp('im shift ')
        for  i = 1:Npos
            fprintf(' %i / %i \n  ', i, Npos)      
            data.scan{i}.data = imshift_fast(data.scan{i}.data, xshift, yshift, [], 'nearest');
            data.scan{i}.noise = imshift_fast(data.scan{i}.noise, xshift, yshift,[],  'nearest');
            data.scan{i}.mask = imshift_fast(data.scan{i}.mask, xshift, yshift,[], 'nearest')>0;
        end
        
       
        
        probe_positions = [];
        for i = 1:Npos
           probe_positions(i,:) = data.scan{i}.pos;
        end
%         keyboard
        
        if time >= 736220.63445056719  % I rotated smaract stage !!!
            probe_positions = -probe_positions;
        end
        
        [Nx_prob, Ny_prob] = size( data.scan{1}.data);
   
        disp('More Cropping / binning')
        %% crop data to center for further processing
        binning = 1 ;
        cropping = 1;
        diffraction_pattern= zeros(Nx_prob/binning/cropping, Ny_prob/binning/cropping, Npos, 'single');
        for i = 1:Npos%  memory issues!!! 
            d = rescale_fft(  data.scan{i}.data  , 1/cropping);
            diffraction_pattern(:,:,i) = rescale_img( d , 1/binning);  
%             
%             imagesc(d)
%             drawnow
            data.scan{i}.data = [];
            
        end
                
        noise_pattern = zeros(Nx_prob/binning/cropping, Ny_prob/binning/cropping, Npos, 'single');
        for i = 1:Npos%  memory issues!!! 
            d = rescale_fft(  data.scan{i}.noise  , 1/cropping);
            noise_pattern(:,:,i) = rescale_img(d , 1/binning);  
           data.scan{i}.noise = [];
        end
        mask_oversat = false(Nx_prob/binning/cropping, Ny_prob/binning/cropping, Npos);
        for i = 1:Npos%  memory issues!!! 
            d = rescale_fft(  data.scan{i}.mask  , 1/cropping);
            mask_oversat(:,:,i) = rescale_img(d , 1/binning) > 0;  
            data.scan{i}.mask = [];
        end
%         for i = 1:Npos%  memory issues!!! 
%             diffraction_pattern(:,:,i) = replace_nans(diffraction_pattern(:,:,i),  mask_oversat(:,:,i) );
%             noise_pattern(:,:,i) = replace_nans(noise_pattern(:,:,i),  mask_oversat(:,:,i) );
%         end
    
        clear data

        disp('done')

%         disp('Removing background')
%         diffraction_pattern = diffraction_pattern - quantile(diffraction_pattern(1:4:end), 1e-3);

%         disp('calculating errors')
        
%         keyboard
        
        
%         posit = @(x)((x+abs(x))/2);
%         for i = 1:Npos%  memory issues!!! 
%             N = sqrt(noise_pattern(:,:,i).^2+setup.Background.ShotNoise.^2);
%              N = sqrt(posit(diffraction_pattern(:,:,i)  + N )) - ...
%                 sqrt(posit(diffraction_pattern(:,:,i)  - N )); 
%             N(isnan(N)) = 0;
%             noise_pattern(:,:,i) = N;
%         end
% 
%         for i = 1:Npos   %  memory issues!!! 
%             diffraction_pattern(:,:,i) = sqrt(posit(diffraction_pattern(:,:,i)));
%         end

%      pos_range = [quantile(probe_positions(:,1), 0.1), quantile(probe_positions(:,1), 0.9), ...
%          quantile(probe_positions(:,2), 0.1), quantile(probe_positions(:,2), 0.9)];
%      dist = [pos_range(2) - pos_range(1), pos_range(4) - pos_range(3)];
%           
%      wrong = [probe_positions(:,1) < pos_range(1) - dist(1)*0.2 | ...
%          probe_positions(:,1) > pos_range(2) + dist(1)*0.2 | ...
%          probe_positions(:,2) < pos_range(3) - dist(2)*0.2 | ...
%          probe_positions(:,2) > pos_range(4) + dist(2)*0.2 ];
% 
%      diffraction_pattern(:,:,wrong) = [];
%      noise_pattern(:,:,wrong) = [];
%      mask_oversat(:,:,wrong) = [];
%      probe_positions(wrong,:) = [];
     
     disp('fixing some errors of the pixis camera ')
      diffraction_pattern(1:2,:,:) = 0;

            
    disp('saving')
    [pathstr,name,ext] = fileparts(filename) ;
     save('-v7.3', ['data/fast_cache/', name, '-cache'], 'diffraction_pattern', 'noise_pattern', 'probe_positions', 'mask_oversat', 'setup', 'time')
end


    %%%%%%%%%%%%%%%%%
      cropping = 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if cropping > 1
        disp('cropping')
        diffraction_pattern = rescale_fft(diffraction_pattern, 1/cropping);
        noise_pattern = rescale_fft(noise_pattern,1/cropping);
        mask_oversat = rescale_fft(mask_oversat, 1/cropping);  
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
%     extra_binning = 1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            binning = setup.Binning * extra_binning;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
        [Np_p(1), Np_p(2), Npos] = size( diffraction_pattern);
        Np_p = Np_p / extra_binning;

        disp(['data collected at: ' , datestr(time, 'dd-mm-yy hh:MM')])
%         keyboard
        
   
        
            
%         noise_pattern = ones(Nx_prob, Ny_prob, Npos, 'single')*nanmin(diffraction_pattern(diffraction_pattern > 0));
%         keyboard


    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % %% depend on te camera orientation !!! 
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    diffraction_pattern=rot90(diffraction_pattern,1);
    noise_pattern=rot90(noise_pattern,1);
    mask_oversat=rot90(mask_oversat,1);
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       
        Npix_all = 1024;
        NA_0 = 0.8; % NA of full CCD , NA lens ~0.4 
        NA = NA_0 / Npix_all * mean(Np_p) * binning;
        scale = lambda/(2*NA); %% it should be half period 
        fprintf('NA = %3.2g \nPixel size %3.3g nm\n', NA,scale*1e9 )
%       keyboard
%  
%  7.9339e-07
%  
%  keyboard
%  
%  xxxx


        
        
% %     
%         for i = 1:Npos
%             imagesc( log(1+   diffraction_pattern(:,:,i)), [0, log(max(diffraction_pattern(:)))])
%             pause(0.1)
%         end      

        scale = scale / user_scale_size;
        fprintf('Corrected pixel size %3.3g nm\n', scale*1e9 )
        probe_positions = probe_positions *1e-9/scale;

%         keyboard
        
        probe_D =  130e-6; % 120e-6;
       % probe_D =  50e-6; % 120e-6;
        
        sample_probe_dist = 0.001e-3; % for visible data
       
        oversample = (probe_D/scale) / mean(Np_p)
        


        probe =  get_probe(Np_p, probe_D, scale);
        probe = near_field_evolution(probe, sample_probe_dist, lambda, Np_p*scale);
        
%         C = 0.1; %  R^2 / Z  -- for very very near field data
%         X = (-Np_p(1)/2:Np_p(1)/2-1);
%         Y = (-Np_p(2)/2:Np_p(2)/2-1);
%         [X,Y] = meshgrid(X,Y);
%         R = exp(2i.*(X.^2+Y.^2)/mean(Np_p) .* C);
%         probe = probe .* R;
%         
        
         
        figure
        imagesc_hsv(probe)
        title(num2str(sample_probe_dist*1e6))
        pause(0.1)
  
        
        
min_noise = min(noise_pattern,[],3);
min_noise = min(min_noise(min_noise> 0));
noise_pattern = noise_pattern +min_noise;

        
disp('rotating data !! ')

rot = 1;  % pro ty stara data ... 
for i = 1:Npos
    diffraction_pattern(:,:,i) = rot90(diffraction_pattern(:,:,i)',rot);
    noise_pattern(:,:,i) = rot90(noise_pattern(:,:,i)',rot);
    mask_oversat(:,:,i) = rot90(mask_oversat(:,:,i)',rot);
end


%  keyboard

disp('fixing oversaturation ')

% diffraction_pattern = replace_nans(diffraction_pattern, rescale_fft(rescale_fft(mask_oversat,1/2),2))

%% replace overaturated centers by a smooth guess 
% [~,ROI] = crop_image(ones(Np_p),8);
% diffraction_pattern(ROI{:},:) = replace_nans(diffraction_pattern(ROI{:},:),mask_oversat(ROI{:},:), 'nearest'); 

total_diff =  nanmean(diffraction_pattern.^2, 3);
disp('centering')

[~, x, y] = center_pattern(total_diff, 'mass');
                
% keyboard

diffraction_pattern =  imshift_fast( diffraction_pattern, x,y,[], 'nearest');
noise_pattern =  imshift_fast( noise_pattern, x,y, [], 'nearest');
mask_oversat =  imshift_fast( single(mask_oversat), x,y,[], 'nearest') > 0;

% keyboard

mask_oversat(end-4:end,:,:) = false;
mask_oversat = bsxfun(@and, mask_oversat, rescale_fft(ones(Np_p*extra_binning/2),2) | sum(mask_oversat,3)<Npos/2);

%%  binning should be after deconvolution (although binning is also blurring)
if extra_binning > 1
    warning('not binning, downsampling !!! ')
    diffraction_pattern = diffraction_pattern(1:extra_binning:end,1:extra_binning:end,:) ;  
    noise_pattern = noise_pattern(1:extra_binning:end,1:extra_binning:end,:);  
    mask_oversat = mask_oversat(1:extra_binning:end,1:extra_binning:end,:) > 0;  
end


end
    
    
  


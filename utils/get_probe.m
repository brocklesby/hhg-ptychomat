function probe =  get_probe(Np_p, probe_D, px_scale)
% -----------------------------------------------------------------------
% This file is part of the PTYCHOMAT Toolbox
% Author: Michal Odstrcil, 2016
% License: Open Source under GPLv3
% Contact: ptychomat@gmail.com
% Website: https://bitbucket.org/michalodstrcil/ptychomat
% -----------------------------------------------------------------------
% Description: Get a circular probe estimate 

    xgrid = -Np_p(1)/2+1:Np_p(1)/2;
    ygrid = -Np_p(2)/2+1:Np_p(2)/2;
    [X,Y] = meshgrid( xgrid, ygrid );
    R = (probe_D/px_scale)/2;
    probe = single(X.^2+Y.^2 < R^2);
end
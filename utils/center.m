%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.


function [pos_x, pos_y, mu, sigma] = center(X, use_shift)
% Description: find center of mass of matrix X,  calculate variance if
% needed 

    if nargin == 1
        use_shift = true;
    end


    [N,M] = size(X);
    mass = sum(X(:));

    pos_x =  (sum(sum(X,1).*(1:M))/mass);
    pos_y =  (sum(sum(X,2).*(1:N)')/mass);  
    if nargout > 2
        mu = [pos_x, pos_y]';
    end
    
    if nargout == 4
        pos_xx = (sum(sum(X,1).*((1:M)-pos_x).^2)/mass);
        pos_yy = (sum(sum(X,2).*((1:N)-pos_y)'.^2)/mass);
        pos_xy = ((X*((1:M)-pos_x)')'*((1:N)-pos_y)'/mass);
        pos_yx = ((X*((1:M)-pos_x)')'*((1:N)-pos_y)'/mass);
        sigma =  [ pos_xx, pos_xy; pos_yx, pos_yy];
    end
    

    if use_shift
        pos_x = pos_x - M/2;
        pos_y = pos_y - N/2;
    end
              
end
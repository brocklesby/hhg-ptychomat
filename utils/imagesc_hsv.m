%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.

function [cax_out, rgb_data] = imagesc_hsv(varargin)
% Description: imagesc for drawing complex number arrays  

    par = inputParser;
    par.addOptional('data', [])
    par.addParameter('scale',  nan , @isnumeric )
    par.addParameter('clim',  [] , @isnumeric )
    par.addParameter('inverse',  false , @islogical )  % use white background 
    par.addParameter('show_ROI',  false , @islogical )  % show only intersting area
    par.addParameter('points',  [] , @isnumeric )  % plot dots 
    par.addParameter('enhance_contrast',  false , @islogical )  % plot dots 
    par.addParameter('axis',  [] , @isnumeric )  % plot dots 
    par.addParameter('show',  true , @islogical )  % plot dots 
    par.addParameter('colormap',  'rgb' , @isstr )  % rgb / cmyk, cmyk is better for printing 

    par.parse(varargin{:})
    r = par.Results;
    data = r.data;
    clim = r.clim;

    if all(data(:) == 0)
        warning('Empty data to plot')
        return 
    end
          
    
    
    [W,H] = size(data);
    
    if ~isempty(r.axis)
        X = linspace(r.axis(1),r.axis(2),W)*1e6;
        Y = linspace(r.axis(3),r.axis(4),H)*1e6;
    else
        if ~isnan(r.scale)
            scale = ones(2,1).*r.scale(:);
            X = [-W/2:W/2-1]* scale(1)*1e6;
            Y = [-H/2:H/2-1]* scale(2)*1e6;
        else
            X = 1:W; Y = 1:H;
        end
    end
    if r.show_ROI
        asum = abs(sum(data,3));
        try           
            T1 =  (graythresh_new((sum(asum,1))));
            T2 =  (graythresh_new((sum(asum,2))));
            asum(:,sum(asum,1) < T1) = 0;
            asum(sum(asum,2) < T2,:) = 0;
            [~,ROI] = get_ROI(asum > 0.01*quantile(asum(:), 0.99), 0);
            data = data(ROI{:});
            X = X(ROI{1});
            Y = Y(ROI{2});
        catch
            warning('ROI estimation failed')
        end
    end
    [W,H] = size(data);

    if ~isempty(clim)
        ind_min = abs(data) < clim(1); 
        ind_max = abs(data) > clim(2);
        data(ind_min) =  data(ind_min) ./ abs(data(ind_min)) * clim(1);
        data(ind_max) =  data(ind_max) ./ abs(data(ind_max)) * clim(2);
    end
    
    adata = abs(data);
  
    
    
    eps  = 1e-4;
    MAX = quantile(adata(:), 1-eps);
    ind = adata > MAX;
    data(ind) = MAX * data(ind) ./ abs(data(ind));
    if r.enhance_contrast
      data = data ./ sqrt(eps+abs(data));
      clim = sqrt(clim);
    end

    
    adata = abs(data);
    if isempty(clim)
        range = sp_quantile(adata(:), [1e-2, 1-1e-2],10);
    else
        range =  clim;
    end

    adata = (adata  - range(1) ) ./ ( range(2) - range(1) );
    ang_data = angle(data); 
      
    if r.enhance_contrast 
        ang_range = max(abs(sp_quantile(ang_data(:), [1e-2, 1-1e-2],10)));
        ang_range = max(1e-3, ang_range);
        ang_data = 2*pi*ang_data  ./ (2* ang_range);
    end
       
    
    if r.inverse
        hue =  mod(ang_data+1.5*pi, 2*pi)/(2*pi);
        hsv_data = [ hue(:) , adata(:), ones(W*H,1) ];
    else 
        hue =  mod(ang_data+2.5*pi, 2*pi)/(2*pi);
        hsv_data = [ hue(:) , ones(W*H,1), adata(:) ];
    end
    hsv_data = min(max(0, hsv_data),1);

    if strcmp(r.colormap, 'cmyk')  % better for printing
        hsv_data(:,1) = mod(hsv_data(:,1)+0.4, 1);
        rgb_data = hsv2cmyk(hsv_data);
    elseif strcmp(r.colormap, 'rgb') % better for screen 
        rgb_data = hsv2rgb(hsv_data);
    end

    rgb_data = reshape(rgb_data, W,H,3);      
    rgb_data = min(1,rgb_data);

%     rgb_data = rgb_data / max(rgb_data(:));
    
    if r.show
        hh = imagesc(Y,X, rgb_data );
        axis image 
    end


    if r.show
        % Get the parent Axes of the image
        cax = ancestor(hh,'axes');

%         if ~isempty(clim)
%             set(cax,'CLim',clim);
%         elseif ~ishold(cax)
%             set(cax,'CLimMode','auto');
%         end
        axis image 

        if ~isempty(r.points)  && ~isnan(r.scale) 
            hold on
                points = r.scale*1e6*r.points;
                plot( points(:,1),points(:,2), '.w')
            hold off
        end
    else
        cax = 0;
    end
    if nargout > 0
        cax_out = cax;
    end

    
end




%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.

function [dX, dY] = get_img_grad(img)
    %% get vertical and horizontal gradient of the image 
    
    import core.*
    
    Np = size(img);
    
    if nargout == 1
        fX = fft(img,[],2);
        X = (fftshift((0:Np(2)-1)/Np(2))-0.5);
        dX = bsxfun(@times, fX,2i*pi*X);
        dX = ifft(dX,[],2);
        return
    end 
    if nargout > 1
        X = (fftshift((0:Np(2)-1)/Np(2))-0.5);
        Y = (fftshift((0:Np(1)-1)/Np(1))-0.5)';

        % use matlab implicite GPU paralelization 
        if isa(img, 'gpuArray')
            % it is much faster to use 2D fft for GPU despite higher
            % computational costs 
            img = fft2(img);
            % make it slightly faster with GPU 
            [dX, dY] = Gfun(@multiply_gfun, img, X, Y);
            dX = ifft2(dX);
            dY = ifft2(dY);
        else
            fX = fft(img,[],2);
            fY = fft(img,[],1);
            dX = bsxfun(@times, fX,2i*pi*X);
            dY = bsxfun(@times, fY,2i*pi*Y);
            dX = ifft(dX,[],2);
            dY = ifft(dY,[],1);
        end
    end

   
end
% 
    
function [dX, dY]=multiply_gfun(img, X,Y)
    dX = img .* (2i*pi)* X;
    dY = img .* (2i*pi)* Y;
end

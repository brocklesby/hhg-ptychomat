%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.

function positions = make_circular_scan(scan_area, step, round_step,  square_scan, odd_sort )
% Description: make a circular scanning pattern 


    if nargin < 4
        square_scan = true;  %  do a square scan but for price of more movements
    end
    if nargin < 3
        round_step = step / 10; %  round the moevements to minimize the motor pos error
    end
    if nargin < 5
        odd_sort = true;   % do a more robust version resistent to power fluctuations 
    end
    
    x0 = [mean(scan_area(1:2)), mean(scan_area(3:4))];

    Nfold = 7; %  7

    positions = zeros(0,2);
    radius = zeros(0,1);
    
    N = -1;
    i = 0;
    while  (N+Nfold  < length(positions) || i < 3)
        N = length(positions);
        r = i*step;
        for j  = 1:max(i*Nfold,1)
            x = x0 + r*[sind(2*i+j * 360 / max(1,i*Nfold)), cosd(2*i+j * 360 / max(1,i*Nfold))];
            if x(1) < scan_area(1) ||  x(1) > scan_area(2) || x(2) < scan_area(3) ||  x(2) > scan_area(4)
                if square_scan
                    continue
                else
                    break
                end
            end
            if i > 0
                dx = x-positions(end, :);
                ind = abs(dx) < round_step; 
                if any(ind)
                    dx(~ind) = dx(~ind) + abs(dx(ind))/2  *-sign(dx(~ind));  % make ot cropped offset more pseudo random 
                    dx(ind) = 0; 
                end
                positions(end+1, :) =  positions(end, :)+dx;
            else
                positions(end+1,:)=x;
            end
%             
%             plot(positions(:,1), positions(:,2), '.'); 
%             pause(0.1)
%                 
            radius(end+1) = i;
        end
        if ~square_scan && j < max(i*Nfold,1) 
            break
        end
        i = i+1;
    end
    if ~square_scan
        positions(radius == radius(end),:) = [];
        radius(radius == radius(end)) = [];
    end
        
    if odd_sort
        [~, odd_sort] = sort(mod(radius,2));
        positions = positions(odd_sort, :);
    end
    



end

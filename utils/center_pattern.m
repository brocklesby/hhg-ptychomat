function [pattern, pos_x, pos_y] = center_pattern(pattern, type, shift_type)
% -----------------------------------------------------------------------
% This file is part of the PTYCHOMAT Toolbox
% Author: Michal Odstrcil, 2016
% License: Open Source under GPLv3
% Contact: ptychomat@gmail.com
% Website: https://bitbucket.org/michalodstrcil/ptychomat
% -----------------------------------------------------------------------
% Description: shift the pattern to be centered



    if nargin  < 2 || isempty(type)
        type = 'clever';
    end
    if nargin < 3
        shift_type =  'nearest';
    end
    [pos_x, pos_y] = find_center(pattern, type);

    
    pattern = imshift_fast(pattern, pos_x, pos_y, [], shift_type); 
    
    
end

function pattern_new = rescale_fft(pattern, scale, boundary_value)
% -----------------------------------------------------------------------
% This file is part of the PTYCHOMAT Toolbox
% Author: Michal Odstrcil, 2016
% License: Open Source under GPLv3
% Contact: ptychomat@gmail.com
% Website: https://bitbucket.org/michalodstrcil/ptychomat
% -----------------------------------------------------------------------
% Description: a simple function to tile / crop array with 2^N size


    if scale == 1
        pattern_new =  pattern;
        return
    end
    
    if nargin < 4
        boundary_value = 0;
    end

    [W,H,N] = size(pattern);
    if all(mod([W,H],1/scale) ~= 0 )
        error('Wrong image size, it cannot be divided by %i', 1/scale)
    end



    if ~islogical(pattern ) || any(boundary_value ~= 0)
        pattern_new = bsxfun(@times, ones( ceil(W*scale), ceil(H*scale), N, class(pattern) ) , boundary_value);
    else
        pattern_new = false( ceil(W*scale), ceil(H*scale), N);
    end
    

    if scale < 1
        W = W*scale;
        H = H*scale;
        pattern_new = pattern( round(end/2+(-W/2+1:W/2)), round(end/2+(-H/2+1:H/2)), :);
    else
        pattern_new(round(end/2+(-W/2+1:W/2)), round(end/2+(-H/2+1:H/2)), :) = pattern;
    end

end


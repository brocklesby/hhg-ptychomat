function img_new = rescale_img(img, scale, type)
% -----------------------------------------------------------------------
% This file is part of the PTYCHOMAT Toolbox
% Author: Michal Odstrcil, 2016
% License: Open Source under GPLv3
% Contact: ptychomat@gmail.com
% Website: https://bitbucket.org/michalodstrcil/ptychomat
% -----------------------------------------------------------------------
% Description: a simple funtion that can be used to rescale image of size 2^n


[W,H, N]= size(img);
 
if ~exist('type' , 'var') || scale < 1
    type = 'conv';
end

if scale == 1
    img_new =  img;
    return
end

    if ~islogical(img )
        img_new  = zeros(ceil(W * scale), ceil(H * scale), N, class(img));
    else
        img_new = zeros( ceil(W * scale), ceil(H * scale), N, 'single');
    end
    img = single(img);


    switch type
        case 'fft'
            fimg = fft2(img);
            fimg = fftshift_2D(fimg);
            fimg = rescale_fft(fimg, scale);
            fimg = ifftshift_2D(fimg);
            img_new =  ifft2(fimg) * scale^2 ; 
        case 'conv'
            for i = 1:N

            if scale  < 1
                M = (mean2(img([1,end],:,i)) + mean2(img(:,[1,end],i)))/2;  %  "remove" boundary effects 
                img_tmp = conv2( img(:,:,i)-M , ones(1/scale, 1/scale)/(1/scale)^2,  'same');
                img_new(:,:,i) = img_tmp(1:1/scale:end, 1:1/scale:end)+M;
            else
                img_new(2:scale:end, 2:scale:end,i) = img(:,:,i) ;
                img_new(:,:,i) = conv2( img_new(:,:,i) , ones(scale, scale), 'same');
            end
        end
    end

if isreal(img)
    img_new = real(img_new);
end

end

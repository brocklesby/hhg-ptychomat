%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.

function y = sumsq_cell(x)
% Description: sum incoherently cells x 
% Note: sorry for this ugly workaround but I was not able to make it fast on GPU using matlab with a nicer code 


    N = length(x);
    if N < 9 && builtin( 'isa', x{1}, 'gpuArray' )
       switch N
           case 1, fun = @sum_1; 
           case 2, fun = @sum_2; 
           case 3, fun = @sum_3; 
           case 4, fun = @sum_4; 
           case 5, fun = @sum_5; 
           case 6, fun = @sum_6; 
           case 7, fun = @sum_7; 
           case 8, fun = @sum_8; 
       end
        y = arrayfun(fun, x{:}); 
    else
        y = 0;
        for i = 1:N
             y = y + abs(x{i}).^2;
        end
    end
end

% GPU kernel merging 

function y = sum_1(x)
    y = abs(x).^2;
end
function y = sum_2(x1,x2)
    y = abs(x1).^2+abs(x2).^2;
end
function y = sum_3(x1,x2,x3)
    y = abs(x1).^2+abs(x2).^2+abs(x3).^2;
end
function y = sum_4(x1,x2,x3,x4)
    y = abs(x1).^2+abs(x2).^2+abs(x3).^2+abs(x4).^2;
end
function y = sum_5(x1,x2,x3,x4,x5)
    y = abs(x1).^2+abs(x2).^2+abs(x3).^2+abs(x4).^2+abs(x5).^2;
end
function y = sum_6(x1,x2,x3,x4,x5,x6)
    y = abs(x1).^2+abs(x2).^2+abs(x3).^2+abs(x4).^2+abs(x5).^2+abs(x6).^2;
end
function y = sum_7(x1,x2,x3,x4,x5,x6,x7)
    y = abs(x1).^2+abs(x2).^2+abs(x3).^2+abs(x4).^2+abs(x5).^2+abs(x6).^2+abs(x7).^2;
end
function y = sum_8(x1,x2,x3,x4,x5,x6,x7,x8)
    y = abs(x1).^2+abs(x2).^2+abs(x3).^2+abs(x4).^2+abs(x5).^2+abs(x6).^2+abs(x7).^2+abs(x8).^2;
end


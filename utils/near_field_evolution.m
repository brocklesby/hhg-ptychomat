function [u_1, H, h] = near_field_evolution(u_0, z, lambda, extent, varargin)
    
    p = inputParser;
    %   Make input string case independant
    p.CaseSensitive = false;
    %   Specifies the required inputs
    addRequired(p,'u_0',@isnumeric);
    addRequired(p,'z',@isscalar);
    addRequired(p,'lambda',@isscalar);
    addRequired(p,'extent',@isnumeric);

    %   Create optional inputs
    addParamValue(p,'always_nearfield', false,@islogical);

    parse(p,u_0, z, lambda, extent,varargin{:});
    r = p.Results;
    

    extent = extent(:) .* [1,1]';
    if z == 0
        H = 1;
        h = [];
        u_1 = u_0;
        return
    end
    if z == inf
        H = [];
        h = [];
        u_1 = [];
        return
    end
    if isvector(u_0)
        u_0 = u_0(:);
    end
        
    Npix = size(u_0);
   

    xgrid = 0.5+(-Npix(1)/2:Npix(1)/2-1);
    ygrid = 0.5+(-Npix(2)/2:Npix(2)/2-1);
    k = 2 * pi / lambda(1);
    
    %% Fresnel number    
    F = mean(extent)^2 /   (lambda(1) * z * mean(Npix) ); 
%     fprintf('Fresnel number/Npix %3.3g\n', F)
    
    if abs(F) < 1 && ~r.always_nearfield
       
        %% calculate the propagation in the farfield regime => nearfield + apply low pass filter 
        warning('Farfield regime, F/Npix=%g', F )
        Xrange = xgrid*extent(1)/Npix(1);
        Yrange = ygrid*extent(2)/Npix(2);
        [X,Y] = meshgrid(Xrange, Yrange);
        h = exp( 1i*k*z +1i*k/(2*z) * (X'.^2 + Y'.^2));
        H = ifftshift(fft2(fftshift(h))); %  /mean(Npix)
        H = H /  abs(H(end/2+1, end/2+1)); % renormalize to conserve flux in image 
                
    else
        %% standard ASM 
        kx =  2 * pi .*xgrid / extent(1) ;
        ky =  2 * pi .*ygrid / extent(2);
        [Kx, Ky] = meshgrid(kx, ky);
%     Kx = imshift_fft(Kx,1,1);
%     Ky = imshift_fft(Ky,1,1);
        K = (Kx'.^2+Ky'.^2);

        % low angle approximation 
        H = exp( 1i*k*z +-1i*z*K/(2*k));
%         H = exp( 1i*k*z +-1i*z*(Kx'.^2+Ky'.^2)/(2*k));
        h = [];
    end
    
    if isvector(u_0)
        H = H(:,end);
        try
            h = h(:,end); 
        end
    end
        
    u_1 = ifft2( bsxfun(@times, fftshift(H), fft2(u_0)));
    
%     H = imshift_fast(H,-1,-1);
%     imagesc(angle(fftshift(fft2(H))));
    
    
end





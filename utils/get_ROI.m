%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.


function [ROI, range,  coord] = get_ROI(mask, extent, type)
% Description: find optimal rectange containing the mask 


    if all(mask(:) == 0)
        error('Empty mask')
    end

    if nargin < 3
        type = 'any';
    end
    if nargin == 1  
       extent = 0.25;
    end

    [W,H] = size(mask);
    if ~islogical(mask)
       error('Not implemented')
    end

    x = any(mask,2);
    y = any(mask,1);
    coord = [find(x, 1,'first'), find(x, 1,'last'), find(y, 1,'first'), find(y, 1,'last')];

    w = (coord(2) - coord(1));
    h = (coord(4) - coord(3));
    Cx = (coord(2) +coord(1))/2;
    Cy = (coord(4) + coord(3))/2;


    coord(1) =  floor(Cx  - ceil( (0.5 + extent) *w )) ;
    coord(2) =  ceil(Cx  + ceil((0.5 + extent) * w )) ;
    coord(3) =  floor(Cy  - ceil((0.5 + extent) * h )) ;
    coord(4) =  ceil(Cy  + ceil((0.5 + extent) * h )) ;

    switch type
        case 'odd'
            coord(2) = coord(1) + floor((coord(2) - coord(1))/2)*2;
            coord(4) = coord(3) + floor((coord(4) - coord(3))/2)*2;
        case 'even'
            coord(2) = coord(1) + ceil((coord(2) - coord(1))/2)*2-1;
            coord(4) = coord(3) + ceil((coord(4) - coord(3))/2)*2-1;
        case 'pow2'
            w = 2^(nextpow2( coord(2) - coord(1))-2);
            h = 2^(nextpow2( coord(4) - coord(3))-2);
            Cx = (coord(2) +coord(1))/2;
            Cy = (coord(4) + coord(3))/2;
            coord(1) =  round(Cx  - w) ;
            coord(2) =  round(Cx  + w-1) ;
            coord(3) =  round(Cy  - h) ;
            coord(4) =  round(Cy  + h-1) ;   
    end
    
    coord(coord < 1) = 1;
    if coord(2) > W ; coord(2) = W; end
    if coord(4) > H ; coord(4) = H; end

    range = {coord(1):coord(2), coord(3):coord(4)};


    ROI = false(size(mask));
    ROI(range{1}, range{2}) =  true;
 
        
end

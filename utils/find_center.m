function [pos_x, pos_y] = find_center(pattern, type)
% -----------------------------------------------------------------------
% This file is part of the PTYCHOMAT Toolbox
% Author: Michal Odstrcil, 2016
% License: Open Source under GPLv3
% Contact: ptychomat@gmail.com
% Website: https://bitbucket.org/michalodstrcil/ptychomat
% -----------------------------------------------------------------------
% Description: several wayes to find a center of a pattern 


    if ~isreal(pattern)
        pattern = abs(pattern);
    end
    pattern = pattern / max(pattern(:));

    [m,n] = size(pattern);

    switch type
        case 'max'
           [pos_y, pos_x] = argmax(pattern);
            pos_x = pos_x - floor(n/2)-1;
            pos_y = pos_y  - floor(m/2)-1;
                       
        case 'mass'
            [pos_x, pos_y]  = center(pattern);
            pos_x = pos_x - (m/2 - ceil(m/2))-1;
            pos_y = pos_y - (n/2 - ceil(n/2))-1;

        case 'clever'
            alpha = 2;
            % sometimes not so robust ... 
            pattern = pattern / max(abs(pattern(:)));
            pattern = denoise(pattern, 0.5); % avoid some error with threshilding
            thresh = exp(graythresh_new(log(0.0001+abs(pattern(pattern~=0)))));
            mask = abs(pattern) > thresh;
            
            mask = crop_outliers(mask);
            pattern(~mask) = 0;
            [pos_x, pos_y]  = center(pattern.^alpha);
            pos_x = pos_x - (m/2 - ceil(m/2))-1;
            pos_y = pos_y - (n/2 - ceil(n/2))-1;

        case 'hybrid'
            [c,r] = argmax(pattern);

           try               
                R = 3;
                pattern = pattern(c + (-R:R), r + (-R:R));
                pattern = pattern - min(pattern(:));
                pattern  =  pattern.^2 ;  % !!!! 
                [pos_x, pos_y] = center( pattern );
            catch
                pos_x = 0.5;
                pos_y = 0.5;
            end
            pos_x = pos_x - 1 + r - floor(n/2)-0.5;
            pos_y = pos_y - 1 + c - floor(m/2)-0.5;

            
    end

    
end

%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.


function [img, gamma] = stabilize_phase(img, varargin)
% Description:  adjust phase of the object to be mostly around zero and

% img - complex image to stabilize
% img_ref - complex images used as reference, if empty ones is used 
% weights  - bool array denoting the region of interest 
% split  - split FFTon smaller blocks on GPU 

    import math.*
    import utils.*
    

    par = inputParser;
    par.addOptional('img_ref',  [] )
    par.addOptional('weights',  1 )

    par.parse(varargin{:})
    r = par.Results;
    img_ref = r.img_ref; 
    weights = r.weights; 
    
    [M,N,~] = size(img);

    if isempty(img_ref)
        img_ref = ones(M,N,class(img));
    end
    if isempty(weights)
        weights = true(M,N);
    end
    
    %% calculate the optimal phase shift 
    phase_diff = conj(img) .* img_ref;
    gamma = mean2(phase_diff .*weights) ./ mean2(weights);
    gamma = gamma./abs(gamma);
    
    img =img .* gamma;
       

end

%*-----------------------------------------------------------------------*
%|                                                                       |
%|  Except where otherwise noted, this work is licensed under a          |
%|  Creative Commons Attribution-NonCommercial-ShareAlike 4.0            |
%|  International (CC BY-NC-SA 4.0) license.                             |
%|                                                                       |
%|  Copyright (c) 2017 by Paul Scherrer Institute (http://www.psi.ch)    |
%|                                                                       |
%|       Author: CXS group, PSI                                          |
%*-----------------------------------------------------------------------*
% You may use this code with the following provisions:
%
% If the code is fully or partially redistributed, or rewritten in another
%   computing language this notice should be included in the redistribution.
%
% If this code, or subfunctions or parts of it, is used for research in a 
%   publication or if it is fully or partially rewritten for another 
%   computing language the authors and institution should be acknowledged 
%   in written form in the publication: “Data processing was carried out 
%   using the “cSAXS matlab package” developed by the CXS group,
%   Paul Scherrer Institut, Switzerland.” 
%   Variations on the latter text can be incorporated upon discussion with 
%   the CXS group if needed to more specifically reflect the use of the package 
%   for the published work.
%
% A publication that focuses on describing features, or parameters, that
%    are already existing in the code should be first discussed with the
%    authors.
%   
% This code and subroutines are part of a continuous development, they 
%    are provided “as they are” without guarantees or liability on part
%    of PSI or the authors. It is the user responsibility to ensure its 
%    proper use and the correctness of the results.


function [oROI, oROI_vec, sub_px_shift] = find_reconstruction_ROI( positions,Np_o, Np_p )
% Description: precalculate the reconstruction regions for CPU and GPU 

    positions = positions(:,[2,1]); 
    positions = bsxfun(@plus, positions, ceil(Np_o/2-Np_p/2)); 
    sub_px_shift = positions - round(positions); 
    
    sub_px_shift = sub_px_shift(:,[2,1]);  % return to the original XY coordinates 
    
    positions = round(positions); 
    
    

    range = ([min(positions(:,1)), max(positions(:,1))+ Np_p(1), min(positions(:,2)), max(positions(:,2))+ Np_p(2)]);
    if range(1) < 1 || range(2) > Np_o(1) || range(3) < 1 || range(4) > Np_o(2)
        warning('Object support is too small, not enough space for probes !! ') 
        keyboard   
    end
    
    oROI = cell(2,1);
    for dim = 1:2
        oROI{dim} = [positions(:,dim),positions(:,dim)+ Np_p(dim)-1]-1;
        oROI{dim} = uint32(oROI{dim}); 
    end
    
    if nargout > 1
        Npos = length(positions);
        oROI_vec = cell(Npos,2);
        for ii = 1:Npos
            for i = 1:2
                oROI_vec{ii,i} = oROI{i}(ii,1):oROI{i}(ii,2);
            end
        end
    end
        
        
end




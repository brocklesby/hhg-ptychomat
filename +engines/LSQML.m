% LSQML is an example of implementation of the Least squared maximum
% likelihood method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018


% Academic License Agreement
%
% Source Code
%
% Introduction 
% •	This license agreement sets forth the terms and conditions under which the PAUL SCHERRER INSTITUT (PSI), CH-5232 Villigen-PSI, Switzerland (hereafter "LICENSOR") 
%   will grant you (hereafter "LICENSEE") a royalty-free, non-exclusive license for academic, non-commercial purposes only (hereafter "LICENSE") to use the cSAXS 
%   ptychography MATLAB package computer software program and associated documentation furnished hereunder (hereafter "PROGRAM").
%
% Terms and Conditions of the LICENSE
% 1.	LICENSOR grants to LICENSEE a royalty-free, non-exclusive license to use the PROGRAM for academic, non-commercial purposes, upon the terms and conditions 
%       hereinafter set out and until termination of this license as set forth below.
% 2.	LICENSEE acknowledges that the PROGRAM is a research tool still in the development stage. The PROGRAM is provided without any related services, improvements 
%       or warranties from LICENSOR and that the LICENSE is entered into in order to enable others to utilize the PROGRAM in their academic activities. It is the 
%       LICENSEE’s responsibility to ensure its proper use and the correctness of the results.”
% 3.	THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR 
%       A PARTICULAR PURPOSE AND NONINFRINGEMENT OF ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. IN NO EVENT SHALL THE LICENSOR, THE AUTHORS OR THE COPYRIGHT 
%       HOLDERS BE LIABLE FOR ANY CLAIM, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE 
%       OF THE PROGRAM OR OTHER DEALINGS IN THE PROGRAM.
% 4.	LICENSEE agrees that it will use the PROGRAM and any modifications, improvements, or derivatives of PROGRAM that LICENSEE may create (collectively, 
%       "IMPROVEMENTS") solely for academic, non-commercial purposes and that any copy of PROGRAM or derivatives thereof shall be distributed only under the same 
%       license as PROGRAM. The terms "academic, non-commercial", as used in this Agreement, mean academic or other scholarly research which (a) is not undertaken for 
%       profit, or (b) is not intended to produce works, services, or data for commercial use, or (c) is neither conducted, nor funded, by a person or an entity engaged 
%       in the commercial use, application or exploitation of works similar to the PROGRAM.
% 5.	LICENSEE agrees that it shall make the following acknowledgement in any publication resulting from the use of the PROGRAM or any translation of the code into 
%       another computing language:
%       "Data processing was carried out using the cSAXS ptychography MATLAB package developed by the Science IT and the coherent X-ray scattering (CXS) groups, Paul 
%       Scherrer Institut, Switzerland."
%
% Additionally, any publication using the package, or any translation of the code into another computing language should cite for difference map:
% P. Thibault, M. Dierolf, A. Menzel, O. Bunk, C. David, F. Pfeiffer, High-resolution scanning X-ray diffraction microscopy, Science 321, 379–382 (2008). 
%   (doi: 10.1126/science.1158573),
% for mixed coherent modes:
% P. Thibault and A. Menzel, Reconstructing state mixtures from diffraction measurements, Nature 494, 68–71 (2013). (doi: 10.1038/nature11806),
% for LSQ-ML method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018
% for OPRP method 
%  M. Odstrcil, P. Baksh, S. A. Boden, R. Card, J. E. Chad, J. G. Frey, W. S. Brocklesby,  "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369
% 6.	Except for the above-mentioned acknowledgment, LICENSEE shall not use the PROGRAM title or the names or logos of LICENSOR, nor any adaptation thereof, nor the 
%       names of any of its employees or laboratories, in any advertising, promotional or sales material without prior written consent obtained from LICENSOR in each case.
% 7.	Ownership of all rights, including copyright in the PROGRAM and in any material associated therewith, shall at all times remain with LICENSOR, and LICENSEE 
%       agrees to preserve same. LICENSEE agrees not to use any portion of the PROGRAM or of any IMPROVEMENTS in any machine-readable form outside the PROGRAM, nor to 
%       make any copies except for its internal use, without prior written consent of LICENSOR. LICENSEE agrees to place the following copyright notice on any such copies: 
%       © All rights reserved. PAUL SCHERRER INSTITUT, Switzerland, Laboratory for Macromolecules and Bioimaging, 2017. 
% 8.	The LICENSE shall not be construed to confer any rights upon LICENSEE by implication or otherwise except as specifically set forth herein.
% 9.	DISCLAIMER: LICENSEE shall be aware that Phase Focus Limited of Sheffield, UK has an international portfolio of patents and pending applications which relate 
%       to ptychography and that the PROGRAM may be capable of being used in circumstances which may fall within the claims of one or more of the Phase Focus patents, 
%       in particular of patent with international application number PCT/GB2005/001464. The LICENSOR explicitly declares not to indemnify the users of the software 
%       in case Phase Focus or any other third party will open a legal action against the LICENSEE due to the use of the program.
% 10.	This Agreement shall be governed by the material laws of Switzerland and any dispute arising out of this Agreement or use of the PROGRAM shall be brought before 
%       the courts of Zürich, Switzerland. 

function [self, cache, fourier_error] =  LSQML(self,par,cache,fourier_error,iter)
    global gpu 
    import core.*

    % define some useful variables 
    psi = cell(par.Nmodes, 1);
    aprobe = abs(self.probe{1}(:,:,1,1)); % update only once per iteration 
    Nobjects = length(self.object);
    object_upd_sum = cell(Nobjects,1);
    obj_illum_sum = cell(Nobjects,1);
    obj_proj = cell(par.Nobjects,1);

    for ll = 1:Nobjects
        obj_illum_sum{ll} = Gzeros(self.Np_o);
        object_upd_sum{ll} = Gzeros(self.Np_o, true);
    end
    for ll = 1:par.Nobjects
        obj_proj{ll} = Gzeros([self.Np_p, par.grouping], true);
    end
    %%%%%%%%%%%%%%%%%% LSQ-ML algorithm %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     

    % avoid unimportant warnings
    warning('off','MATLAB:nearlySingularMatrix')
    
    probe = self.probe;

%    for ML is more useful to get close/overlapping positions 
%          use already precalculated indices 
    if is_method(par, 'MLs')
        % load sparse indices
        rand_ind = randi(length( cache.preloaded_indices_sparse));
        indices = cache.preloaded_indices_sparse{rand_ind}.indices;
        scan_ids = cache.preloaded_indices_sparse{rand_ind}.scan_ids;
    else % load compact indices 
        indices = cache.preloaded_indices_compact{1}.indices;
        scan_ids = cache.preloaded_indices_compact{1}.scan_ids;
    end
    
    Nind = length(indices);
    if is_method(par, 'MLs')
        % shuffle order
        ind_range = randperm(Nind);
    else
        % MLc, call groups in given order, they are sorted by size to make
        % execution on GPU more effecient + stable convergence 
        ind_range = 1:Nind;
    end
     
    
   %% apply updated in parallel over sets indices{jj}
   for  jj = ind_range
       % list of positions solved in current subiteration 
        g_ind = indices{jj}; 
        
        for ll = 1:max(par.Nprobes, par.Nobjects) 
            % generate indices of the used probes 
            % single probe only
            p_ind{ll} = 1;
        end
        
        %% load data to GPU 
        modF = get_modulus(self, cache, g_ind);
 
        % get objects projections 
        for ll = 1:par.Nobjects
            obj_proj{ll} = get_projections(self.object{ll}, obj_proj{ll},ll, g_ind, cache);
        end
        % get illumination probe 
        for ll = 1:par.Nprobes
            if (ll == 1 && (par.variable_probe || par.variable_intensity))
                % add variable probe (OPRP) part into the constant illumination 
                probe{ll} =  get_variable_probe(self.probe{ll}, self.probe_evolution(g_ind,:),p_ind{ll});
            else
                %store the normal (constants) probe(s)
                probe{ll} = self.probe{min(ll,end)}(:,:,min(end,p_ind{ll}),1);
            end
        end

        for ll = 1:max(par.Nprobes, par.Nobjects)  % Nlayers 
            %% fourier propagation  
            if (ll == 1 && par.apply_subpix_shift) 
                probe{ll}  = shift_probe(probe{ll}, self.modes{min(end,ll)}.sub_px_shift(g_ind,:) );
            end
            % get projection of the object and probe 
            psi{ll} = bsxfun(@times, obj_proj{min(ll,end)}, probe{ll});
            % propagate, store to psi{ll} to save memory 
            psi{ll} = fwd_fourier_proj(psi{ll});            
        end
        
        % get intensity (modulus) on detector including different corrections
        aPsi = get_reciprocal_model(self, psi, par);
            

        if (mod(iter,min(20, 2^(floor(2+iter/50)))) == 0 || iter < 10) || par.verbose_level > 2  % calculate only sometimes to make it faster
            [fourier_error(iter,g_ind)] = get_fourier_error(modF, aPsi);
            if any(~isfinite(fourier_error(iter,g_ind)))
                error('Convergence failed, error contains NaNs, quitting ... ')
            end
        end
        
        % get update vector after optimization of the reciprocal space constraint 
        chi = modulus_constraint(modF,aPsi,psi,0,1);  
        % the transposed linear correction model would be applied here 
        
        % backpropagate to the realspace 
        for ll = 1:max(par.Nprobes, par.Nobjects)
            % to avoid memory allocation chi variable is used in Fourier and real space 
            chi{ll} = back_fourier_proj(chi{ll});
        end

        
        %%%%%%%%%%%% LSQ optimization code (probe & object updates)%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        
        for ll = 1:max(par.Nprobes, par.Nobjects)
            object_reconstruct = iter >= par.object_reconstruct && (par.apply_multimodal_update ||  ll <= par.Nobjects );
            probe_reconstruct = iter >= par.probe_reconstruct;
            
           llo = min(par.Nobjects, ll);
           llp = min(par.Nprobes, ll);

           if probe_reconstruct
                %% update probe 
                probe_update = Gfun(@get_update, chi{ll}, obj_proj{llo});
                if (ll == 1 && par.apply_subpix_shift) 
                    probe_update  = shift_probe(probe_update , -self.modes{min(end,ll)}.sub_px_shift(g_ind,:) );
                end
                
                % calculate single update for all current positions 
                %m_probe_update = mean(probe_update,3) ; 
                
               % BETTER WAY: assume that sum(|obj_proj|^2,3) is close to 1 
               % and additionally use weighting based on confidence given by illum_sum_0
                % => apriory weighting giving less importance to the less
                % illuminated regions
                weight_proj = cache.illum_sum_0{1} ./ (cache.illum_sum_0{1}+0.01*cache.MAX_ILLUM(1));
                weight_proj = get_projections(weight_proj,[],1, g_ind, cache);
                m_probe_update = mean( weight_proj.* probe_update,3);

            else
                m_probe_update = 0;
            end
            
            if object_reconstruct
                %% update object
                % get gradient descent update 
                object_update_proj = Gfun(@get_update, chi{ll}, probe{llp});              

                % calculate update direction
                % apply "overlap" constraint to get better estimate
                % of the update directin 
                for ll_tmp = 1:Nobjects
                    if is_method(par, 'MLs')
                        object_upd_sum{ll_tmp}(:) = eps*1i;
                    end
                    if par.delta_p == 0 
                        %no preconditioner as in the original ML method 
                        object_upd_sum{ll_tmp} = set_projections(object_upd_sum{ll_tmp},object_update_proj ,1, g_ind, cache);
                        object_update_proj = get_projections(object_upd_sum{ll_tmp},object_update_proj,ll_tmp, g_ind, cache);                       
                    else
                        % damped LSQ method (preconditioned update)
                        object_update_proj  = bsxfun(@times, object_update_proj , aprobe);
                        object_upd_sum{ll_tmp} = set_projections(object_upd_sum{ll_tmp},object_update_proj,1, g_ind, cache);
                        object_upd_precond = Gfun(@object_sum_update_Gfun, object_upd_sum{ll_tmp}, cache.illum_sum_0{ll_tmp},cache.MAX_ILLUM(ll_tmp)*(par.delta_p));
                        object_update_proj = get_projections(object_upd_precond,object_update_proj,ll_tmp, g_ind, cache);
                        if is_method(par, 'MLs'); object_upd_sum{ll_tmp} = object_upd_precond; end
                    end
                end
            else 
                object_update_proj = 0;
            end
            
                       
            %% calculation of the LSQ optimal step 
            if  par.beta_LSQ > 0 &&  ll == 1   && object_reconstruct && probe_reconstruct 
                if ~isempty(gpu); wait(gpu); end 
                % prevent ill posed inversion, ideally it should be Garray(mean(abs(AA1)+abs(AA4))/2) but it i show  
                lambda =  eps(single(1)) / prod(self.Np_p);
                [AA1,AA2,AA4, Atb1,Atb2] = ...
                        Gfun(@get_optimal_step_lsq, chi{ll},object_update_proj,m_probe_update,...
                        obj_proj{llo},probe{llp}, lambda); 
                
                AA1 = sum2(AA1); 
                AA2 = sum2(AA2); 
                AA4 = sum2(AA4); 
                Atb1 = sum2(Atb1); 
                Atb2 = sum2(Atb2);
                
                % it seems faster to solve it on GPU than using pagefun on GPU 
                AA1 = Ggather(AA1);AA2 = Ggather(AA2);AA4 = Ggather(AA4);Atb1 = Ggather(Atb1);Atb2 = Ggather(Atb2);
                AA3 = conj(AA2);
                lambda = 0.1;  % add some small regularization to avoid unstabilities
                I = lambda*[mean(AA1), mean(AA4)];

                AA = [ AA1+I(1), AA2; AA3, AA4+I(2)];
                Atb= [ Atb1; Atb2]; 
                
                % solve the system of equations 
                LSQ_step = Gpagefun(@mldivide,AA, Atb);
                
                LSQ_step = Ggather(LSQ_step);

                % prevent unwanted oscilation of step gets too high 
                cache.beta_probe(g_ind) =  ((par.beta_probe *par.beta_LSQ)*LSQ_step(2,1,:));
                cache.beta_object(g_ind) = ((par.beta_object*par.beta_LSQ)*LSQ_step(1,1,:));
            elseif par.beta_LSQ == 0 &&  ll == 1
                cache.beta_probe(g_ind)  = par.beta_probe;
                cache.beta_object(g_ind)  = par.beta_object;
            elseif ll == 1
                % computationally cheaper method that assumes only
                % diagonall terms of the AA matrix 
                [cache.beta_probe(g_ind),cache.beta_object(g_ind)] = ...
                    gradient_projection_solver(self,chi{ll},obj_proj{min(end,ll)},probe{ll},...
                        object_update_proj, m_probe_update,g_ind, par, cache);
                cache.beta_probe(g_ind) =   (par.beta_probe *par.beta_LSQ)*cache.beta_probe(g_ind);
                cache.beta_object(g_ind) =  (par.beta_object*par.beta_LSQ)*cache.beta_object(g_ind);                
            end
            

            if any(g_ind == self.Npos) && ll == 1   
               %  show the optimal steps calculation 
               if par.verbose_level  > 3
                    if mod(iter, 5) == 0
                        figure(121122)
                        plot(squeeze(real(cache.beta_object)))
                        hold all 
                        plot(squeeze(real(cache.beta_probe)))
                        hold off 
                        legend({'O', 'P'})
                        title('Update step for each scan position')
                        xlabel('Scan positions #')
                        ylabel('Step')
                        drawnow
                    end
               end
                verbose(1,'Average step p%i: %3.3g  o%i: %3.3g \n ', llp,mean(cache.beta_probe(g_ind)),llo,mean(cache.beta_object(g_ind)));
            end

            %%%%%%%%%%%%%%% apply update with the optimal LSQ step %%%%%%%%%%%%%%%%%
            if object_reconstruct && is_method(par, 'MLs')  % update now only for MLs method -> faster convergence 
                self.object = update_object(self,self.object, object_upd_sum, llo, {g_ind}, scan_ids(jj), par, cache);
            end
            if probe_reconstruct && ll <= max(par.Nprobes)
                self.probe{ll} = update_probe(self.probe{ll}, m_probe_update, par,  g_ind, cache);
                self.probe{ll} = apply_probe_contraints(self.probe{ll}, self.modes{ll});
            end
            
            
            %%%%%%%%%%%%% update other parameters of the ptychography model
            if iter >= par.probe_pos_search && ll == 1 
                % find optimal position shift that minimize chi{1} in current iteration 
                self.modes{1} = gradient_position_solver(chi{1}, obj_proj{1},probe{1},self.modes{1}, g_ind);
            end
            if (par.variable_probe || par.variable_intensity) && iter > par.probe_reconstruct+1 && ll == 1 
                % approximation of the ORTHOGONAL PROBE RELAXATION (OPR) - allow variable probe wavefront 
                %  Odstrcil, M., et al. "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369.
                % iterate over all sub probes 
                [self.probe{ll}, self.probe_evolution] = ...
                    update_variable_probe(self, self.probe{ll}, self.probe_evolution, m_probe_update,probe_update, obj_proj{ll}, chi{ll},cache.illum_sum_0{ll}, p_ind{ll}, g_ind, cache, par);
            end
            %% other parameters are similar ... 
      

        end

   end
   
   % applying single update emulates behaviour of the original ML method ->
   % provides better noise robustness 
   % advantage is that less memory is needed and no linesearch is required
   
    if object_reconstruct && is_method(par, 'MLc')
        self.object = update_object(self, self.object, object_upd_sum, llo, indices, scan_ids, par, cache);
    end

end

%% auxiliary functions related to LSQ-ML code 

function object = update_object(self, object, object_upd_sum,llo, g_ind, scan_ids, par, cache)
    import core.*
    obj_ids = unique([scan_ids{:}]);
    % take single optimal value, works well for most of samples 
    % it is possible to use different weighting for each scan position but
    % it may become less stable in some cases -> robusness is preferred 
    
    % in case of the MLc method take minimum of the LSQ updates from all
    % subsets 
    

    if is_method(par, 'MLc')
        % preconditioner should be applied on the total sum of all object_upd_sum
        for kk = obj_ids
            if par.delta_p > 0
                object_upd_sum{kk} = Gfun(@object_sum_update_Gfun, object_upd_sum{kk}, cache.illum_sum_0{kk},cache.MAX_ILLUM(kk)*(par.delta_p));
            end
        end
        for i = 1:length(g_ind)
            beta_object(i) = median(cache.beta_object(g_ind{i}));
        end
        beta_object=min(beta_object);
    else
        % for MLs use median of the calculated step values 
        beta_object = median(cache.beta_object(g_ind{1}));
    end
    
    % perform the update 
    for kk = obj_ids
        object{kk} =  object{kk}+object_upd_sum{kk}*beta_object; 
    end
    
    if par.verbose_level > 3
        % show applied subsets (update amplitude) and probe update amplitude 
        figure(11)
        subplot(1,2,1)
        cla()
        imagesc(abs(object_upd_sum{1}), [0,quantile(abs(object_upd_sum{1}(:)), 1-1e-3)]);
        colormap bone 
        axis image 
        hold all
        fprintf('Object update norm: %g\n', norm2(object_upd_sum{1}(cache.object_ROI{:})))
        
        for k = 1:length(g_ind)
            for i = unique(scan_ids{k})
                plot(self.probe_positions_0(g_ind{k}(scan_ids{k}==i),1)+self.Np_o(2)/2,self.probe_positions_0(g_ind{k}(scan_ids{k}==i),2)+self.Np_o(1)/2, '.')
            end
        end
        title('Amplitude of the object update')
    end
            
    
end
function  probe = update_probe(probe, m_probe_update, par, g_ind, cache)
    import core.*
    %% update probe 
    
    if ndims(probe)  < 4
        % most simple case, no multiprobe needed 
        probe = probe + m_probe_update .* mean(cache.beta_probe(g_ind));
    else
        % variable probe extension with shared probe 
        probe(:,:,1,1) = probe(:,:,1,1) + m_probe_update .* mean(cache.beta_probe(g_ind));
    end
        
    if par.verbose_level > 3
        % show applied subsets (update amplitude) and probe update
        % amplitude 
        figure(11)
        subplot(1,2,2)
        imagesc((abs(m_probe_update)))
        axis off image 
        title('Amplitude of the probe update')
        colormap bone 
        drawnow 
    end
        
end


function img = shift_probe(img, shift)
    % subpixel probe shifting 
    import core.*

    if all(shift(:) == 0); return ; end 
    global use_gpu

     shift = single(shift);
    x = reshape(shift(:,1),1,1,[]);
    y = reshape(shift(:,2),1,1,[]);
       

    Np = size(img);

    if size(img,3) ~= size(shift,1) && use_gpu
        % ugly trick making matlab GPU FFT faster 
        img = repmat(img,1,1,size(shift,1));
    end
    img = fft2(img);

    if Np(1) ~= Np(2); error('Not implemented'); end
    grid = Garray(fftshift((0:Np(1)-1)'/Np(1))-0.5);
    
    if use_gpu
        img = Gfun(@apply_shift_Gfun,img,x,y,grid', grid);
    else
        img = bsxfun(@times, img, exp((-2i*pi)*bsxfun(@times, x,grid')));
        img = bsxfun(@times, img, exp((-2i*pi)*bsxfun(@times,y,grid)));
    end
    img = ifft2(img); 
end

%% merged CUDA kernels for faster calculations 
function [AA1,AA2,AA4, Atb1,Atb2] = ...
            get_optimal_step_lsq(chi,dO,dP,O,P, lambda)
     % fast kernel for estimation of optimal probe and object steps 
    dOP = dO.*P;
    dPO = dP.*O;
    cdOP = conj(dOP);
    cdPO = conj(dPO);

    AA1 = real(dOP .* cdOP)+lambda;
    AA2 = (dOP .* cdPO);
    AA4 = real(dPO .* cdPO)+lambda;
    Atb1 = real(cdOP .* chi);
    Atb2 = real(cdPO .* chi);
end

function update = get_update(chi, proj)
    update = chi .* conj(proj); 
end

function object_upd_sum = object_sum_update_Gfun(object_upd_sum, obj_illum_sq_sum, max)
    object_upd_sum = object_upd_sum ./ (obj_illum_sq_sum+ max);
end
function  img = apply_shift_Gfun(img,x,y,xgrid, ygrid)
    img = img .* exp((-2i*pi)*(x*xgrid+y*ygrid)); 
end


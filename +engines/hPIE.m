function [self, cache, fourier_error] =  hPIE(self,par,cache,fourier_error,iter)
        global gpu 
        import core.*

        if par.variable_probe
            % expand the probe to the full size
            self.probe{1} = reshape(self.probe{1},prod(self.Np_p),[]);
            self.probe{1} = reshape(self.probe{1} * self.probe_evolution', self.Np_p(1), self.Np_p(2), []);
        end
        
        psi = cell(par.Nmodes, 1);
        Psi = cell(par.Nmodes, 1);
        pos_correction = iter >= min([par.probe_pos_search]); 
        probe_max = cell(par.Nprobes,1); 
        object_max = cell(par.Nobjects,1);
        aprobe2 = abs(mean(self.probe{1},3)).^2; % update only once per iteration 
                
        for ll = 1:(par.Nobjects*par.Nscans)
            obj_illum_sum{ll} = Gzeros(self.Np_o);
            object_upd_sum{ll} = Gzeros(self.Np_o, true);
        end
        for ll = 1:par.Nprobes
            probe_upd_sum{ll}= (1+1i)*1e-8; 
            probe_illum_sum{ll} = 1e-8; 
        end

        
        for ll = 1:par.Nobjects
            obj_proj{ll} = complex(Gzeros([self.Np_p, par.grouping]));
        end
        
        %%%%%%%%%%%%%%%%%% ePIE algorithm %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for ll = 1:par.Nprobes
            probe_max{ll} = Ggather(max2(abs(self.probe{ll}))).^2;
        end
        for ll = 1:par.Nobjects
            object_max{ll} = Ggather(max2(abs(self.object{ll}(cache.object_ROI{:})))).^2;
        end
        
        if any(isnan(object_max{1})) ||  any(isnan(probe_max{1}))            
            error('Object or probe is nan, try smaller grouping')
        end
    

    
        % use already precalculated indices 
        rand_ind = randi(length( cache.preloaded_indices_sparse));
        indices = cache.preloaded_indices_sparse{rand_ind}.indices;


       for  ind_ii = randperm(length(indices))
            g_ind = indices{ind_ii}; 
            for ll = 1:max(par.Nprobes, par.Nobjects) 
                % generate indices of the used probes 
                if par.variable_probe && ll == 1
                    p_ind{ll} = g_ind;
                else  % single probe only
                    p_ind{ll} = 1;
                end
            end

            %% load data to GPU (if not loaded yet)
            modF = get_modulus(self, cache, g_ind);
            % get objects projections 
            for ll = 1:par.Nobjects
                obj_proj{ll} = get_projections(self.object{ll}, obj_proj{ll},ll, g_ind, cache);
            end
            % get illumination probe 
            for ll = 1:par.Nprobes
                %store the normal (constants) probe(s)
                probe{ll} = self.probe{min(ll,end)}(:,:,min(end,p_ind{ll}),1);
            end
            for ll = 1:max(par.Nprobes, par.Nobjects)  % Nlayers 
                if (ll == 1 && par.apply_subpix_shift)
                    probe{ll}  = shift_probe(probe{ll}, self.modes{min(end,ll)}.sub_px_shift(g_ind,:) );
                end
                % get objects projections            
                obj_proj{ll} = get_projections(self.object{ll}, obj_proj{ll},ll, g_ind, cache);

                % get projection of the object and probe 
                psi{ll} = bsxfun(@times, obj_proj{min(ll,end)}, probe{min(ll,end)});
                %% fourier propagation  
                psi{ll} = fwd_fourier_proj(psi{ll} , self.modes{min(end, ll)});            
            end
            
        
            % get intensity (modulus) on detector including different corrections
            aPsi = get_reciprocal_model(self, psi, par);
                              
            %%%%%%%%%%%%%%%%%%%%%%% LINEAR MODEL CORRECTIONS END %%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%            
            if (mod(iter,min(20, 2^(floor(2+iter/50)))) == 0 || (iter < 20))  % calculate only sometimes to make it faster
                [fourier_error(iter,g_ind)] = get_fourier_error(modF, aPsi);
            end

            chi = modulus_constraint(modF,aPsi,psi, 0, 1);

            % backpropagate to the realspace 
            for ll = 1:max(par.Nprobes, par.Nobjects)
                % to avoid memory allocation chi variable is used in Fourier and real space 
                chi{ll} = back_fourier_proj(chi{ll});
            end
        
            for ll = 1:max(par.Nprobes, par.Nobjects)
                    
                %% get optimal get_grad_lsqient lenghts 
                 object_update=0; probe_update=0;
                
                 if iter >= par.object_reconstruct && (ll <= par.Nobjects || par.apply_multimodal_update)
                     object_update = Gfun(@get_grad_lsq,chi{ll}, probe{min(ll,end)},...
                        probe_max{min(end,ll)}(1,1,min(end,p_ind{ll})),par.delta_p);
                 end
                
                
                if iter >= par.probe_reconstruct && ll <= par.Nprobes
                    %% find optimal probe update !!! 
                        probe_update = Gfun(@get_grad_lsq,chi{ll},obj_proj{min(end,ll)},...
                            object_max{min(ll,end)}, par.delta_p);  % 
                end

                if ll == 1 &&  (par.beta_LSQ || pos_correction) 
                    %% variable step extension, apply only the first mode except the 3PIE case 
                    %% it will use the same alpha for the higher modes !!!!    
                    
                    
                    [cache.beta_probe(g_ind),cache.beta_object(g_ind)] =  gradient_projection_solver(self,chi{ll},obj_proj{min(end,ll)},probe{ll},...
                        object_update, probe_update,g_ind, par, cache);
                     if any(g_ind ==1)
                         verbose(1,'Average alpha p:%3.3g  o:%3.3g \n ', mean(cache.beta_probe),mean(cache.beta_object));
                    end
                    if pos_correction && any(ismember(par.position_solver, 'get_grad_lsqient'))
                        self.modes{ll} = gradient_position_solver(chi{ll}, obj_proj{min(end,ll)},probe{ll},self.modes{ll}, g_ind);
                    end
                end
                     

                
                
                %%%%%%%%%%%%%%%%%%%%% PROBE UPDATE   %%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%
                %% update probe first 
                if (iter >= par.probe_reconstruct) && ll <= par.Nprobes
                      
                    % only in case of first layer probe, otherwise update interprobes
                    beta_probe = get_vals(cache.beta_probe,g_ind);                       
                            
                    %%%%%%%%%%% update probe %%%%%%%%%%%%%%%%%%%%%%%%%%
                    probe{ll} = Gfun(@upd_probe_Gfun,probe{ll},probe_update, beta_probe); 

                    if (ll == 1 && par.apply_subpix_shift) 
                        probe{ll}  = shift_probe(probe{ll} , -self.modes{min(end,ll)}.sub_px_shift(g_ind,:));
                    end   


                    if iter >= par.probe_reconstruct  
                        if (par.variable_probe && ll == 1 && par.variable_probe_modes > 0)
                            if length(p_ind{ll}) == par.grouping  % ugly trick to make it faster, avoid some CUDA issue 
                                self.probe{ll}(:,:,p_ind{ll}) = probe{ll};  % slowest line for large datasets !!!!!
                                if par.use_gpu; wait(gpu); end
                            end
                        else
                            self.probe{ll} = mean(probe{ll},3); % merge information from all the shifted  probes if needed
                        end
                    end
 
                end
                             
                
                %%%%%%%%%%%%%%%%%%%%% OBJECT UPDATE   %%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%
                if iter >= min([par.object_reconstruct]) && ....
                        ( ll <= par.Nobjects || par.apply_multimodal_update )
                   
                    if iter >= par.object_reconstruct 
                    
                        beta_object =  get_vals(cache.beta_object,g_ind) ;
                      
                        object_update = bsxfun(@times, object_update, beta_object);                                                

                        obj_illum_sum{ll}(:) = 0;
                        object_upd_sum{ll}(:) = eps*1i;
                        object_update = bsxfun(@times,  object_update,  aprobe2); % make is more like dumped LSQ solution 
                        [object_upd_sum{ll},obj_illum_sum{ll}] = set_projections_rc(object_upd_sum{ll},obj_illum_sum{ll}, object_update,aprobe2,ll, g_ind, cache);
                         self.object{ll} = Gfun(@object_update_Gfun, self.object{ll},object_upd_sum{ll}, obj_illum_sum{ll}, cache.MAX_ILLUM(ll));

                    end                   
                end
           

            
            end       
       end
          
 
        if par.variable_probe && par.variable_probe_modes > 0 &&  iter >= par.probe_reconstruct
            self.probe{1} = Ggather(self.probe{1});
            [self.probe{1}, self.probe_evolution] = apply_SVD_filter(self.probe{1}, par.variable_probe_modes+1,  self.modes{1});
            self.probe{1} = Garray(self.probe{1});
        elseif ( ~isempty(self.probe_support)) &&  iter >= par.probe_reconstruct
            self.probe{1} = apply_probe_contraints(self.probe{1}, self.modes{1});
        end
                
end

function get_grad_lsq = get_grad_flat(chi,proj,max2, delta )
%   ePIE method
    get_grad_lsq =  (1/max2) * chi .* conj(proj) ;
end

function get_grad_lsq = get_grad_lsq(chi,proj,max2, delta )
    % dumped LSQ method  
    aproj = abs(proj) ;
    get_grad_lsq =  chi .*  conj(proj) .* aproj ./ (aproj.^2 + delta*max2)./sqrt(max2);
end

function object = object_update_Gfun(object,object_upd_sum, obj_illum_sum, max)
    object = object +  object_upd_sum ./ (obj_illum_sum+1e-9* max);
end
function img = shift_probe(img, shift)
    % fast subpixel probe shifts
    import core.*
    
    if all(shift(:) == 0); return ; end 
    global use_gpu

    shift = Garray(shift);
        
    x = reshape(shift(:,1),1,1,[]);
    y = reshape(shift(:,2),1,1,[]);
       

    Np = size(img);

    if size(img,3) ~= size(shift,1) && use_gpu
        % ugly trick making matlab GPU FFT faster 
        img = repmat(img,1,1,size(shift,1));
    end
    img = fft2(img);

    if Np(1) ~= Np(2); error('Not implemented'); end
    grid = Garray(fftshift((0:Np(1)-1)'/Np(1))-0.5);
    
    if use_gpu
        img = Gfun(@apply_shift_Gfun,img,x,y,grid', grid);
    else
        img = bsxfun(@times, img, exp((-2i*pi)*bsxfun(@times, x,grid')));
        img = bsxfun(@times, img, exp((-2i*pi)*bsxfun(@times,y,grid)));
    end
    img = ifft2(img); 
end

function  img = apply_shift_Gfun(img,x,y,xgrid, ygrid)
    img = img .* exp((-2i*pi)*(x*xgrid+y*ygrid)); 
end
function probe = upd_probe_Gfun(probe,probe_update, alpha_p)
    probe =  probe + alpha_p.*probe_update;
end
function array = get_vals(array, ind)
    if isscalar(array)
        return
    else
        array = reshape(array(ind),1,1,[]);
    end
end

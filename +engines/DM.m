% simple DM algorithm with multimodal support 

% Academic License Agreement
%
% Source Code
%
% Introduction 
% •	This license agreement sets forth the terms and conditions under which the PAUL SCHERRER INSTITUT (PSI), CH-5232 Villigen-PSI, Switzerland (hereafter "LICENSOR") 
%   will grant you (hereafter "LICENSEE") a royalty-free, non-exclusive license for academic, non-commercial purposes only (hereafter "LICENSE") to use the cSAXS 
%   ptychography MATLAB package computer software program and associated documentation furnished hereunder (hereafter "PROGRAM").
%
% Terms and Conditions of the LICENSE
% 1.	LICENSOR grants to LICENSEE a royalty-free, non-exclusive license to use the PROGRAM for academic, non-commercial purposes, upon the terms and conditions 
%       hereinafter set out and until termination of this license as set forth below.
% 2.	LICENSEE acknowledges that the PROGRAM is a research tool still in the development stage. The PROGRAM is provided without any related services, improvements 
%       or warranties from LICENSOR and that the LICENSE is entered into in order to enable others to utilize the PROGRAM in their academic activities. It is the 
%       LICENSEE’s responsibility to ensure its proper use and the correctness of the results.�?
% 3.	THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR 
%       A PARTICULAR PURPOSE AND NONINFRINGEMENT OF ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. IN NO EVENT SHALL THE LICENSOR, THE AUTHORS OR THE COPYRIGHT 
%       HOLDERS BE LIABLE FOR ANY CLAIM, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE 
%       OF THE PROGRAM OR OTHER DEALINGS IN THE PROGRAM.
% 4.	LICENSEE agrees that it will use the PROGRAM and any modifications, improvements, or derivatives of PROGRAM that LICENSEE may create (collectively, 
%       "IMPROVEMENTS") solely for academic, non-commercial purposes and that any copy of PROGRAM or derivatives thereof shall be distributed only under the same 
%       license as PROGRAM. The terms "academic, non-commercial", as used in this Agreement, mean academic or other scholarly research which (a) is not undertaken for 
%       profit, or (b) is not intended to produce works, services, or data for commercial use, or (c) is neither conducted, nor funded, by a person or an entity engaged 
%       in the commercial use, application or exploitation of works similar to the PROGRAM.
% 5.	LICENSEE agrees that it shall make the following acknowledgement in any publication resulting from the use of the PROGRAM or any translation of the code into 
%       another computing language:
%       "Data processing was carried out using the cSAXS ptychography MATLAB package developed by the Science IT and the coherent X-ray scattering (CXS) groups, Paul 
%       Scherrer Institut, Switzerland."
%
% Additionally, any publication using the package, or any translation of the code into another computing language should cite for difference map:
% P. Thibault, M. Dierolf, A. Menzel, O. Bunk, C. David, F. Pfeiffer, High-resolution scanning X-ray diffraction microscopy, Science 321, 379–382 (2008). 
%   (doi: 10.1126/science.1158573),
% for mixed coherent modes:
% P. Thibault and A. Menzel, Reconstructing state mixtures from diffraction measurements, Nature 494, 68–71 (2013). (doi: 10.1038/nature11806),
% for LSQ-ML method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018
% for OPRP method 
%  M. Odstrcil, P. Baksh, S. A. Boden, R. Card, J. E. Chad, J. G. Frey, W. S. Brocklesby,  "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369
% 6.	Except for the above-mentioned acknowledgment, LICENSEE shall not use the PROGRAM title or the names or logos of LICENSOR, nor any adaptation thereof, nor the 
%       names of any of its employees or laboratories, in any advertising, promotional or sales material without prior written consent obtained from LICENSOR in each case.
% 7.	Ownership of all rights, including copyright in the PROGRAM and in any material associated therewith, shall at all times remain with LICENSOR, and LICENSEE 
%       agrees to preserve same. LICENSEE agrees not to use any portion of the PROGRAM or of any IMPROVEMENTS in any machine-readable form outside the PROGRAM, nor to 
%       make any copies except for its internal use, without prior written consent of LICENSOR. LICENSEE agrees to place the following copyright notice on any such copies: 
%       © All rights reserved. PAUL SCHERRER INSTITUT, Switzerland, Laboratory for Macromolecules and Bioimaging, 2017. 
% 8.	The LICENSE shall not be construed to confer any rights upon LICENSEE by implication or otherwise except as specifically set forth herein.
% 9.	DISCLAIMER: LICENSEE shall be aware that Phase Focus Limited of Sheffield, UK has an international portfolio of patents and pending applications which relate 
%       to ptychography and that the PROGRAM may be capable of being used in circumstances which may fall within the claims of one or more of the Phase Focus patents, 
%       in particular of patent with international application number PCT/GB2005/001464. The LICENSOR explicitly declares not to indemnify the users of the software 
%       in case Phase Focus or any other third party will open a legal action against the LICENSEE due to the use of the program.
% 10.	This Agreement shall be governed by the material laws of Switzerland and any dispute arising out of this Agreement or use of the PROGRAM shall be brought before 
%       the courts of Zürich, Switzerland. 


function [self, cache, psi_dash, fourier_error ] =  DM(self,par,cache,psi_dash,fourier_error, iter)
    import core.* 
    

    psi = cell(par.Nmodes, 1);
    Psi = cell(par.Nmodes, 1);

    %%%%%%%%%%%%%%%%%%%%%%%%% Difference maps algorithm %%%%%%%%%%%%%%%%%%%
    beta =  1;  % DM contstants defaults 
    gamma = 1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
    probe_norm = norm2(self.probe{1}); 
    Nobjects = length(self.object); 

    for ll = 1:par.Nobjects
        obj_proj{ll} = Gzeros([self.Np_p, par.grouping], true);
    end
    for ll = 1:Nobjects
        obj_illum{ll} = Gzeros(self.Np_o);
        obj_update{ll} = Gzeros(self.Np_o, true);
    end
    for ll = 1:par.Nprobes
        probe_illum{ll} = Gzeros(size(self.probe{ll}));
        probe_update{ll} = Gzeros(size(self.probe{ll}), true);
    end

    indices_0 = 1:self.Npos; 
    for i = 1:ceil(self.Npos/par.grouping)
        indices{i} = indices_0(1+(i-1)*par.grouping : min(end,i*par.grouping));
    end
    
    for jj = 1:length(indices)
        ii = indices{jj};
        modF = get_modulus(self, cache, ii);
         % load on GPU if needed 
        for ll = 1:max(par.Nprobes, par.Nobjects)
            psi_dash{ll,jj} = Garray(psi_dash{ll,jj});
        end
        for ll = 1:par.Nobjects
            obj_proj{ll} = get_projections(self.object{ll}, obj_proj{ll},ll, ii, cache);
        end

        for ll = 1:max(par.Nprobes, par.Nobjects)
             probe{ll} = self.probe{min(ll,end)};
             psi{ll} = bsxfun(@times, obj_proj{min(ll, end)},  probe{ll}); 
            
            %% fourier propagation  
            if iter == 1; psi_dash{ll,jj} = psi{ll}; end % initial guess 
            % P_M (P_0(psi))
            Psi{ll} = Gfun(@DM_update_psi, gamma,psi{ll}, psi_dash{ll,jj} ); 
            Psi{ll} = fwd_fourier_proj(Psi{ll}, self.modes{min(ll,end)} );  
        end  

        aPsi = get_reciprocal_model(self, Psi, par);

        %% get fourier error, but do not waste time in every iteration 
        if mod(iter,min(20, 2^(floor(2+iter/50)))) == 0 || iter < 20 || par.verbose_level > 2
            [fourier_error(iter,ii)] = get_fourier_error(modF, aPsi);
        end
        %% fourier (modulus) contrain 
        Psi = modulus_constraint(modF,aPsi,Psi, par.pfft_relaxation,0);
        for ll = 1:max(par.Nprobes, par.Nobjects)
            Psi{ll} = back_fourier_proj(Psi{ll}, self.modes{min(end,ll)});
            psi_dash{ll,jj}  = Gfun(@DM_update, psi_dash{ll,jj} ,beta, Psi{ll},psi{ll});  
        end
    end
    
    
    
    %% iterative solver of the overlap constraint, important for initial convergence
    for kk = 1:10
        for ll = 1:Nobjects
            obj_illum{ll} = Gzeros(self.Np_o);
            obj_update{ll} = Gzeros(self.Np_o, true);
        end
        for ll = 1:par.Nprobes
            probe_illum{ll} = Gzeros(size(self.probe{ll}));
            probe_update{ll} = Gzeros(size(self.probe{ll}), true);
        end
         
        probe_0 = self.probe; 
        for jj = 1:length(indices)
            for ll = 1:par.Nprobes
                probe{ll} = self.probe{ll};
                cprobe{ll} = conj(probe{ll});
                aprobe{ll} = real(probe{ll}.*cprobe{ll});
            end
        
            % move to GPU (if not there yet)
            for ll = 1:max(par.Nprobes, par.Nobjects)
                psi_dash{ll,jj} = Garray(psi_dash{ll,jj});
            end
            ii = indices{jj};
            for ll = 1:par.Nobjects
                obj_proj{ll} = get_projections(self.object{ll}, obj_proj{ll},ll, ii, cache);
            end
            if iter >= par.probe_reconstruct
                for ll = 1:par.Nprobes   
                    %% update probe 
                    [probe_update{ll},probe_illum{ll}] = QQ_probe(psi_dash{ll,jj}, obj_proj{min(end,ll)}, probe_update{ll},probe_illum{ll});
                end
            end
            if iter >= par.object_reconstruct
                for ll = 1:par.Nobjects
                    %% update object 
                    [obj_update{ll},obj_illum{ll}] = QQ_object(psi_dash{ll,jj}, ii,ll,cache,obj_update{ll},obj_illum{ll}, aprobe{ll}, cprobe{ll});
                end
            end
            % get back from GPU if required 
            if ~par.keep_on_gpu
                for ll = 1:max(par.Nprobes, par.Nobjects)
                    psi_dash{ll,jj} = Ggather(psi_dash{ll,jj});
                end
            end
        end
        for ll = 1:max([par.Nprobes, Nobjects]) 
              if iter >= par.probe_reconstruct && ll <= par.Nprobes
                    % add some inertia to prevent oscilations 
                    self.probe{ll}  = update_probe(self, self.probe{ll} , probe_update{ll} , probe_illum{ll} , par, ll);
              end
              if iter >= par.object_reconstruct && ll <= Nobjects
                    self.object{ll} = Gfun(@update_object, self.object{ll}, obj_update{ll}, obj_illum{ll}, cache.MAX_ILLUM(ll)*1e-4, par.probe_inertia);
              end
        end

        
        dprobe(kk) = max(norm2(self.probe{1} - probe_0{1}) ./ probe_norm);

        verbose(2,'Update probe difference: %3.2f%% \n', dprobe(kk)*100)
        if dprobe(kk) < 0.01 % change is below 1% 
            break
        end
    end
end

function [upd, illum] = QQ_probe_Gfun(psi,proj)
     upd = psi .* conj(proj) ;
     illum = abs(proj).^2;
end

function [probe_update,probe_illum] = QQ_probe(psi, obj_proj, probe_update,probe_illum)
	%% update probe 
    import core.*
    [upd, illum] = Gfun(@QQ_probe_Gfun, psi,obj_proj);
    probe_update = probe_update+sum(upd,3);
    probe_illum = probe_illum+sum(illum,3);
    
end
function [obj_update,obj_illum] = QQ_object(psi, ii,ll,cache,obj_update,obj_illum, aprobe, cprobe)
    %% update object 
    import core.*
    psi = bsxfun(@times, psi, cprobe);
    [obj_update,obj_illum] = set_projections_rc(obj_update,obj_illum, psi,aprobe,ll, ii, cache);
end

function psi = DM_update_psi(gamma,psi, psi_dash )
    % real space update function for difference maps 
    psi = (1+gamma)*psi - gamma*psi_dash;
end
function psi_dash = DM_update(psi_dash,beta, psi_tmp,psi )
    % update funciton for difference maps 
    psi_dash = psi_dash  + beta * (  psi_tmp - psi  ) ;
end


function object = update_object(object, object_upd, object_illum, delta, inertia)
    % apply also some inertia in the object update 
    object = object*inertia + (1-inertia)*object_upd./ (object_illum+delta);
end
function probe = update_probe(self, probe, probe_update, probe_illum, par, probe_id)
    import core.*
    probe_new = probe_update ./  (probe_illum+1e-6); 
    % apply probe support on the first probe mode 
    if probe_id == 1
        probe_new = apply_probe_contraints(probe_new, self.modes{probe_id});
    end
    % add some inertia to prevent oscilations & get faster convergence 
    probe = par.probe_inertia*probe + (1-par.probe_inertia)*probe_new;
end
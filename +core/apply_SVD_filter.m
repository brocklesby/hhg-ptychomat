function [probe, probe_evolution] = apply_SVD_filter(x, Nmodes, mode)
    % the variable probe (OPRP) code
    % find SVD decomposition and limit the probe into several orthogonal
    % modes 
    % additional prior knowledge can be also included 
    
    import core.*
    
    Np = size(x);
         
    [U,S,V] = fsvd(reshape((x),[],Np(3)) ,Nmodes);
     if any(diag(S.^2)/sum(diag(S.^2)) < 2e-3)  % 2e-3 is the weakest that FSVD can recover 
         try
            warning('Running full SVD (maybe use less SVD modes)  (modes %i/%i) ', Ggather(sum(diag(S)/sum(diag(S)) > 2e-3)), Nmodes)
            [U,S,V] = svd(reshape((x),[],Np(3)) ,0);
            U = single(U(:,1:Nmodes));
            S = single(S(1:Nmodes,1:Nmodes));
            V = single(V(:,1:Nmodes));
         catch
             keyboard
         end            
     end


   U = reshape(U, Np(1),Np(2),1,[]);
   U = core.apply_probe_contraints(U, mode);


   V(:,1) = mean(V(:,1)) + 0.99*(V(:,1) - mean(V(:,1)));


    %% remove outliers 
    W = 0.9;
    aV = abs(V); 
    mV = abs(mean(V));
    aV = bsxfun(@minus, aV, mV);
    MAX = sp_quantile(aV,0.99, 4);
    ind = aV > 1.5*MAX;
    aV(ind) = 0;
    aV = bsxfun(@plus, aV, mV);
    %% test median filter to really eliminate the outliers 
    aV = medfilt1(gather(aV),5); 
    if any(ind(:))
        verbose(2,'SVD chronos values corrected %3.2g%% \n', 100*mean(ind(:)))
        V = (W*abs(V) + (1-W)*aV) .* (V./abs(V));
    end

    probe_evolution = real(V*S); 
    avg = mean(probe_evolution(:,1),1); 
    
    probe = U*avg;
    probe_evolution = probe_evolution / avg;


end


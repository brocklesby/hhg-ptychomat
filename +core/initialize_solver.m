% Description: initialization of the ptychography solver, load data to gpu
% and create the reconstruction modes, probes amd objects if not contained
% in self

% Academic License Agreement
%
% Source Code
%
% Introduction 
% •	This license agreement sets forth the terms and conditions under which the PAUL SCHERRER INSTITUT (PSI), CH-5232 Villigen-PSI, Switzerland (hereafter "LICENSOR") 
%   will grant you (hereafter "LICENSEE") a royalty-free, non-exclusive license for academic, non-commercial purposes only (hereafter "LICENSE") to use the cSAXS 
%   ptychography MATLAB package computer software program and associated documentation furnished hereunder (hereafter "PROGRAM").
%
% Terms and Conditions of the LICENSE
% 1.	LICENSOR grants to LICENSEE a royalty-free, non-exclusive license to use the PROGRAM for academic, non-commercial purposes, upon the terms and conditions 
%       hereinafter set out and until termination of this license as set forth below.
% 2.	LICENSEE acknowledges that the PROGRAM is a research tool still in the development stage. The PROGRAM is provided without any related services, improvements 
%       or warranties from LICENSOR and that the LICENSE is entered into in order to enable others to utilize the PROGRAM in their academic activities. It is the 
%       LICENSEE’s responsibility to ensure its proper use and the correctness of the results.”
% 3.	THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR 
%       A PARTICULAR PURPOSE AND NONINFRINGEMENT OF ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. IN NO EVENT SHALL THE LICENSOR, THE AUTHORS OR THE COPYRIGHT 
%       HOLDERS BE LIABLE FOR ANY CLAIM, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE 
%       OF THE PROGRAM OR OTHER DEALINGS IN THE PROGRAM.
% 4.	LICENSEE agrees that it will use the PROGRAM and any modifications, improvements, or derivatives of PROGRAM that LICENSEE may create (collectively, 
%       "IMPROVEMENTS") solely for academic, non-commercial purposes and that any copy of PROGRAM or derivatives thereof shall be distributed only under the same 
%       license as PROGRAM. The terms "academic, non-commercial", as used in this Agreement, mean academic or other scholarly research which (a) is not undertaken for 
%       profit, or (b) is not intended to produce works, services, or data for commercial use, or (c) is neither conducted, nor funded, by a person or an entity engaged 
%       in the commercial use, application or exploitation of works similar to the PROGRAM.
% 5.	LICENSEE agrees that it shall make the following acknowledgement in any publication resulting from the use of the PROGRAM or any translation of the code into 
%       another computing language:
%       "Data processing was carried out using the cSAXS ptychography MATLAB package developed by the Science IT and the coherent X-ray scattering (CXS) groups, Paul 
%       Scherrer Institut, Switzerland."
%
% Additionally, any publication using the package, or any translation of the code into another computing language should cite for difference map:
% P. Thibault, M. Dierolf, A. Menzel, O. Bunk, C. David, F. Pfeiffer, High-resolution scanning X-ray diffraction microscopy, Science 321, 379–382 (2008). 
%   (doi: 10.1126/science.1158573),
% for mixed coherent modes:
% P. Thibault and A. Menzel, Reconstructing state mixtures from diffraction measurements, Nature 494, 68–71 (2013). (doi: 10.1038/nature11806),
% for LSQ-ML method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018
% for OPRP method 
%  M. Odstrcil, P. Baksh, S. A. Boden, R. Card, J. E. Chad, J. G. Frey, W. S. Brocklesby,  "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369
% 6.	Except for the above-mentioned acknowledgment, LICENSEE shall not use the PROGRAM title or the names or logos of LICENSOR, nor any adaptation thereof, nor the 
%       names of any of its employees or laboratories, in any advertising, promotional or sales material without prior written consent obtained from LICENSOR in each case.
% 7.	Ownership of all rights, including copyright in the PROGRAM and in any material associated therewith, shall at all times remain with LICENSOR, and LICENSEE 
%       agrees to preserve same. LICENSEE agrees not to use any portion of the PROGRAM or of any IMPROVEMENTS in any machine-readable form outside the PROGRAM, nor to 
%       make any copies except for its internal use, without prior written consent of LICENSOR. LICENSEE agrees to place the following copyright notice on any such copies: 
%       © All rights reserved. PAUL SCHERRER INSTITUT, Switzerland, Laboratory for Macromolecules and Bioimaging, 2017. 
% 8.	The LICENSE shall not be construed to confer any rights upon LICENSEE by implication or otherwise except as specifically set forth herein.
% 9.	DISCLAIMER: LICENSEE shall be aware that Phase Focus Limited of Sheffield, UK has an international portfolio of patents and pending applications which relate 
%       to ptychography and that the PROGRAM may be capable of being used in circumstances which may fall within the claims of one or more of the Phase Focus patents, 
%       in particular of patent with international application number PCT/GB2005/001464. The LICENSOR explicitly declares not to indemnify the users of the software 
%       in case Phase Focus or any other third party will open a legal action against the LICENSEE due to the use of the program.
% 10.	This Agreement shall be governed by the material laws of Switzerland and any dispute arising out of this Agreement or use of the PROGRAM shall be brought before 
%       the courts of Zürich, Switzerland. 


function [self, cache] = initialize_solver(self,par)
    import core.*
    
    global verbose_level
    verbose_level = par.verbose_level;
        
    first_run = isempty(self.modes) ; % do some more initialization 

    par.Nscans = length(self.reconstruct_ind);


    % get modulus of the measured data 
    Diffraction = fftshift_2D(sqrt(max(0,self.diffraction)));
  
    
    % prepare probe support 
    if isempty(self.probe_support) && ~isempty(par.probe_support_radius)
        % very useful for DM code 
        [X,Y] = meshgrid(-self.Np_p(1)/2+1:self.Np_p(1)/2, -self.Np_p(2)/2+1:self.Np_p(2)/2);
        self.probe_support = sqrt(X.^2+Y.^2) < mean(self.Np_p)/2*par.probe_support_radius; 
    else
        self.probe_support = [];
    end
    
        
 
    if isempty(self.probe_positions)
        self.probe_positions = self.probe_positions_0;
    end
    modes = cell(par.Nmodes,1);
    for i = 1:par.Nmodes
        modes{i}.probe_positions = self.probe_positions;
        modes{i}.probe_support = self.probe_support;
        modes{i}.probe_support_distance = par.probe_support_distance;
    end
       
    object_0 = self.object{1};
    probe_0 = self.probe{1};

    % initialize object
    object{1} = object_0;
    for ll = 1:par.Nobjects
        try
            object{ll} = self.object{ll};
            object{ll}(:);
        catch
            object{i} = exp(0.1i*randn(self.Np_o));
        end
    end

    
    % initialize probe 
    probe_0 = mean(probe_0,3);
    for i = 1:par.Nprobes
        if i == 1
            if  par.variable_probe
               verbose(2,'Creating variable probe \n')
               if ndims(probe_0)  < 4
                   % OPRP methods 
                   probe{i}(:,:,1,1) = probe_0;
                   probe{i}(:,:,1,1+(1:par.variable_probe_modes)) = randc([self.Np_p,1, par.variable_probe_modes]);
               else
                   probe{i} = probe_0;
               end
            else
               probe{i}  = probe_0(:,:,:,1);
            end
        else
           % simply create slightly shifted modes in fourier domain, it is useful for
           % inital guess of incoherent modes after orthogonalization 
           probe{i} = fftshift(imshift_fft(fftshift(probe_0), randn, randn, false));
        end
    end
    
    % orhogonalize probes
    probe = ortho_modes_eig(probe); 
 
   
    self.object = object; 
    self.probe = probe; 
    self.modes = modes; 
    self.diffraction = Diffraction; 
    cache.skip_ind = setdiff(1:self.Npos, [self.reconstruct_ind{:}]); %  wrong datasets  to skip 

    
    if is_method(par, {'PIE', 'ML'})
        % get higly overlapping subsets of indices 
         [cache.preloaded_indices_compact{1}.indices,cache.preloaded_indices_compact{1}.scan_ids] = ...
             get_close_indices(self, cache, par );     

        % precalculate distance matrix for pseudo ePIE / hPIE / MLs to get
        % least overlapping indices 
        for ll = 1:par.Nscans
            dist_mat =  single(distmat(self.probe_positions_0(self.reconstruct_ind{ll},:))); 
            dist_mat(dist_mat==0 |  dist_mat > max(self.Np_p)/2) = inf;
            cache.distances_matrix{ll} = dist_mat;
        end
        for i = 1:min(par.Niter,30)  % preload order of indices 
            [cache.preloaded_indices_sparse{i}.indices,cache.preloaded_indices_sparse{i}.scan_ids] = ...
                get_nonoverlapping_indices(self, cache, par );
        end
    else
        % get just some predefined sets of indices - RAAR, DM , !! order
        % does not matter 
        [cache.preloaded_indices{1}.indices,cache.preloaded_indices{1}.scan_ids] = ...
             get_scanning_indices(self, cache, par );
    end
      
    if isempty(self.probe_evolution)
        % initial coefficients for OPRP approximation 
        self.probe_evolution(:,1) = ones(self.Npos,1);  % first mode is constant 
        self.probe_evolution(:,1+(1:par.variable_probe_modes)) = 1e-6*randn(self.Npos,par.variable_probe_modes);
    end           
    if par.variable_probe && ~is_method(par, {'PIE'})
        pnorm = norm2(self.probe{1});
        self.probe{1}(:,:,:,2:end) = self.probe{1}(:,:,:,2:end) ./ pnorm(1,1,:,2:end);
        self.probe_evolution(:,2:end) = self.probe_evolution(:,2:end) .* mean(pnorm(1,1,:,2:end),3);
    end
        
    Nobjects = length(self.object);
    %% initialize the GPU function 
  
    [cache.oROI_s{1}, cache.oROI{1}] = find_reconstruction_ROI( self.modes{1}.probe_positions,self.Np_o, self.Np_p);

    %% precalculate illumination
    aprobe2 = abs(self.probe{1}(:,:,1)).^2; 
    for ll = 1:Nobjects
        ind = self.reconstruct_ind{ll};
        [cache.oROI_s{1}] = find_reconstruction_ROI( self.modes{1}.probe_positions,self.Np_o, self.Np_p);
        % avoid oscilations by adding momentum term 
        illum_sum_0{ll} = set_projections(Gzeros(self.Np_o), Garray(aprobe2), 1,ind, cache);
    end

    if   ( first_run )  % only in case of the first run !!!! 
        % try to get as good initial guess as possible 
        verbose(2,'Normalize object estimation\n')
        for ll = 1:Nobjects
            if isempty(self.object{ll}); continue; end 
            object_norm(ll) = sqrt(sum2(abs(self.object{ll}).^2 .* illum_sum_0{ll}) ./ sum2(illum_sum_0{ll}));
        end
        verbose(2,'object intensity correction %3.2g \n ', mean(object_norm))

        for ll = 1:Nobjects
            if isempty(self.object{ll}); continue; end 
            self.object{ll} = self.object{ll} ./  mean(object_norm); 
        end

        illum = 0;
        for ll = 1:par.Nprobes
            illum = illum + abs(mean(self.probe{ll}(:,:,:,1),3)).^2;
        end

        corr = median(mean2(illum) ./ mean2(get_modulus(self, cache, 1:self.Npos).^2));

        % farfield 
        corr = corr * prod(self.Np_p); 
        corr = sqrt(corr); % return to amplitude value 
        verbose(2,'probe intensity correction %3.2g \n ', corr)
        for ll= 1:par.Nprobes
            self.probe{ll} = self.probe{ll} ./ corr;
            verbose(2,'Probe %i intensity  %3.2g \n ', ll, norm(self.probe{ll}(:)))
        end
        for ll= 1:Nobjects
            illum_sum_0{ll} = illum_sum_0{ll} / corr.^2;
        end
    end


    for  ll = 1:Nobjects
        illum_sum_0{ll} = Ggather(illum_sum_0{ll}); 
        cache.MAX_ILLUM(ll) = max(illum_sum_0{ll}(:));
        cache.illum_sum_0{ll} = illum_sum_0{ll}; 
    end


    % precalculate illumination ROIs
    cache = precalculate_ROI(self,cache, Ggather(sqrt(aprobe2)));


end

% Perform some basic initial checks before ptychography is started 

% Academic License Agreement
%
% Source Code
%
% Introduction 
% •	This license agreement sets forth the terms and conditions under which the PAUL SCHERRER INSTITUT (PSI), CH-5232 Villigen-PSI, Switzerland (hereafter "LICENSOR") 
%   will grant you (hereafter "LICENSEE") a royalty-free, non-exclusive license for academic, non-commercial purposes only (hereafter "LICENSE") to use the cSAXS 
%   ptychography MATLAB package computer software program and associated documentation furnished hereunder (hereafter "PROGRAM").
%
% Terms and Conditions of the LICENSE
% 1.	LICENSOR grants to LICENSEE a royalty-free, non-exclusive license to use the PROGRAM for academic, non-commercial purposes, upon the terms and conditions 
%       hereinafter set out and until termination of this license as set forth below.
% 2.	LICENSEE acknowledges that the PROGRAM is a research tool still in the development stage. The PROGRAM is provided without any related services, improvements 
%       or warranties from LICENSOR and that the LICENSE is entered into in order to enable others to utilize the PROGRAM in their academic activities. It is the 
%       LICENSEE’s responsibility to ensure its proper use and the correctness of the results.”
% 3.	THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR 
%       A PARTICULAR PURPOSE AND NONINFRINGEMENT OF ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. IN NO EVENT SHALL THE LICENSOR, THE AUTHORS OR THE COPYRIGHT 
%       HOLDERS BE LIABLE FOR ANY CLAIM, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE 
%       OF THE PROGRAM OR OTHER DEALINGS IN THE PROGRAM.
% 4.	LICENSEE agrees that it will use the PROGRAM and any modifications, improvements, or derivatives of PROGRAM that LICENSEE may create (collectively, 
%       "IMPROVEMENTS") solely for academic, non-commercial purposes and that any copy of PROGRAM or derivatives thereof shall be distributed only under the same 
%       license as PROGRAM. The terms "academic, non-commercial", as used in this Agreement, mean academic or other scholarly research which (a) is not undertaken for 
%       profit, or (b) is not intended to produce works, services, or data for commercial use, or (c) is neither conducted, nor funded, by a person or an entity engaged 
%       in the commercial use, application or exploitation of works similar to the PROGRAM.
% 5.	LICENSEE agrees that it shall make the following acknowledgement in any publication resulting from the use of the PROGRAM or any translation of the code into 
%       another computing language:
%       "Data processing was carried out using the cSAXS ptychography MATLAB package developed by the Science IT and the coherent X-ray scattering (CXS) groups, Paul 
%       Scherrer Institut, Switzerland."
%
% Additionally, any publication using the package, or any translation of the code into another computing language should cite for difference map:
% P. Thibault, M. Dierolf, A. Menzel, O. Bunk, C. David, F. Pfeiffer, High-resolution scanning X-ray diffraction microscopy, Science 321, 379–382 (2008). 
%   (doi: 10.1126/science.1158573),
% for mixed coherent modes:
% P. Thibault and A. Menzel, Reconstructing state mixtures from diffraction measurements, Nature 494, 68–71 (2013). (doi: 10.1038/nature11806),
% for LSQ-ML method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018
% for OPRP method 
%  M. Odstrcil, P. Baksh, S. A. Boden, R. Card, J. E. Chad, J. G. Frey, W. S. Brocklesby,  "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369
% 6.	Except for the above-mentioned acknowledgment, LICENSEE shall not use the PROGRAM title or the names or logos of LICENSOR, nor any adaptation thereof, nor the 
%       names of any of its employees or laboratories, in any advertising, promotional or sales material without prior written consent obtained from LICENSOR in each case.
% 7.	Ownership of all rights, including copyright in the PROGRAM and in any material associated therewith, shall at all times remain with LICENSOR, and LICENSEE 
%       agrees to preserve same. LICENSEE agrees not to use any portion of the PROGRAM or of any IMPROVEMENTS in any machine-readable form outside the PROGRAM, nor to 
%       make any copies except for its internal use, without prior written consent of LICENSOR. LICENSEE agrees to place the following copyright notice on any such copies: 
%       © All rights reserved. PAUL SCHERRER INSTITUT, Switzerland, Laboratory for Macromolecules and Bioimaging, 2017. 
% 8.	The LICENSE shall not be construed to confer any rights upon LICENSEE by implication or otherwise except as specifically set forth herein.
% 9.	DISCLAIMER: LICENSEE shall be aware that Phase Focus Limited of Sheffield, UK has an international portfolio of patents and pending applications which relate 
%       to ptychography and that the PROGRAM may be capable of being used in circumstances which may fall within the claims of one or more of the Phase Focus patents, 
%       in particular of patent with international application number PCT/GB2005/001464. The LICENSOR explicitly declares not to indemnify the users of the software 
%       in case Phase Focus or any other third party will open a legal action against the LICENSEE due to the use of the program.
% 10.	This Agreement shall be governed by the material laws of Switzerland and any dispute arising out of this Agreement or use of the PROGRAM shall be brought before 
%       the courts of Zürich, Switzerland. 


function [self,par] = initial_checks(self, par)

if verLessThan('matlab','9.0')
    warning('Code tested only for Matlab 2016a and newer')
end



[self.Np_o(1),self.Np_o(2),~] = size(self.object{1});
[self.Np_p(1),self.Np_p(2),~] = size(self.probe{1});
par.Nrec  = 1;
par.Nscans = length(self.reconstruct_ind);
par.Nobjects = par.Nscans;
par.Nmodes = 1;

Np_d =  size(self.diffraction);
if any(self.Np_p ~= Np_d(1:2)) % && isempty(self.modF_ROI)
    error('Size of probe and data is different')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
par.grouping = min(self.Npos, par.grouping); %  min(10, ceil(self.Npos) / 10);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grouping = round(par.grouping);

global use_gpu gpu 
if use_gpu
    %% !! very empirical estimation of the GPU memory requirements !!!
    memory_extra = (par.keep_on_gpu*(1+2*max(par.Nprobes,par.Nobjects)* is_method(par, {'DM'})) ...
        + 2*par.variable_probe  ); 
    
    % empirically tested maximal number of processed pixels per iteration 
    max_processed_pixels = min(5e7, (gpu.AvailableMemory/8 -  ...
        prod(self.Np_p)*(self.Npos*memory_extra  )/10 )); % 1e7 tested for Titan X 
    
    if is_method(par, {'DM'}) && ~par.keep_on_gpu
        max_processed_pixels = max_processed_pixels / max(par.Nprobes, par.Nobjects);
    end

        
    % safety rule to avoid too large files
    par.grouping = min(par.grouping, ceil(max_processed_pixels / prod(self.Np_p))); % use large grouping -> faster on GPU  
    
    if  is_method(par, {'DM'}) 
        % use large grouping -> faster on GPU
        par.grouping = max(par.grouping, 100*ceil(256^2 / prod(self.Np_p))); 
    end
    
    % adjust grouping to minimize overhead 
    if is_method(par, {'MLs'})
        par.grouping = floor(self.Npos/ceil(self.Npos/par.grouping));
    else
        par.grouping = ceil(self.Npos/ceil(self.Npos/par.grouping));
    end
    
    if par.grouping ~= grouping 
        verbose(1,'Optimal grouping was changed from %i to %i \n', grouping, par.grouping);
    end
    if par.grouping < 1
        error('Too low memory, use smaller dataset or try ePIE')
    end
else
    if is_method(par, {'DM'})
        par.grouping = self.Npos;
    end
end

if par.grouping > 1 &&  is_method(par, {'ePIE'})
    par.grouping = 1;
    warning('Default ePIE uses grouping == 1')
end


if par.variable_probe  && ~is_method(par, { 'ML', 'OPR', 'hPIE'})
    error('Variable probe implemented only for ePIE_OPRP and ML')
end
if par.variable_probe  && is_method(par, { 'ML'}) && par.variable_probe_modes > 1
    warning('ML method allows only single OPR mode')
    par.variable_probe_modes = 1;
end
if ~par.variable_probe 
    par.variable_probe_modes = 0;
end

if islogical(par.beta_LSQ) 
    % use the predictive step a bit smaller than 1 as default 
    par.beta_LSQ = par.beta_LSQ .* 0.9; 
end

%%%%%%%%%  set the multiscan methods %%%%% 
if par.grouping > 1 && par.grouping < self.Npos
    Npos_max = 0; 
    for i = 1:par.Nscans; Npos_max = max(Npos_max,length(self.reconstruct_ind{i}));end
    % auto-adjust size of the groups to fit more to the dataset size, avoid
    % some ineffecient splitting 
    par.grouping = ceil(Npos_max/ceil(Npos_max/par.grouping));

    % auto-adjust size of the groups to fit more to the dataset size 
    par.grouping = ceil(self.Npos/ceil(self.Npos/par.grouping));
    % auto-adjust multiscans to be solved in paralel via PIE methods 
    if is_method(par, {'PIE', 'ML'})
        par.grouping = ceil(par.grouping / par.Nscans) * par.Nscans;
    end
end



%%%%%%%%%%%%%% check if position correction is allowed 
if ~is_method(par, { 'ML'}) && any(isfinite([ par.probe_pos_search])) 
    verbose(2, 'Position correction supported only for PIE/ML methods \n')
    par.probe_pos_search = inf; 
end



%%%%%%%%%%%%%%% OTHER %%%%%%%%%%%%%%%%%%%%


if prod(self.Np_p) *self.Npos >  intmax('int32') && par.keep_on_gpu && is_method(par, {'MLs', 'ePIE'})
    warning('Dataset as more than 2147483647 elements (max of int32), in case of problems try par.keep_on_gpu = false')
end


par.Nrec = 1;


if par.Nrec > max([par.Nmodes, par.Nprobes , par.Nobjects])
    warning('Number of modes is too high')
end

if par.Nmodes < max(par.Nprobes,par.Nobjects)
    verbose(1,'N modes is smaller than N objects / N probes \n')
elseif par.Nmodes > max(par.Nprobes,par.Nobjects)
    error('Nmodes must be <= max(Nprobes, Nobject)')
end
if length(self.probe_positions) ~= self.Npos
   self.probe_positions = [];
end

%%%%%% position correction %%%%% 
if ~is_method(par, {'PIE', 'ML'}) && min( par.probe_pos_search) < par.Niter
    warning('Position corrections works only for PIE/ML methods')
end
if is_method(par, {'PIE', 'ML'}) && min(par.probe_pos_search) < par.Niter && ~(par.apply_subpix_shift)
   warning('Subpixel shifting is strongly recommended for position refinement') 
end

    

end
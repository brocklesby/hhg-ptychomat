% PTYCHO_SOLVER the main loop of ptychography. Call the selected engine 

% Academic License Agreement
%
% Source Code
%
% Introduction 
% •	This license agreement sets forth the terms and conditions under which the PAUL SCHERRER INSTITUT (PSI), CH-5232 Villigen-PSI, Switzerland (hereafter "LICENSOR") 
%   will grant you (hereafter "LICENSEE") a royalty-free, non-exclusive license for academic, non-commercial purposes only (hereafter "LICENSE") to use the cSAXS 
%   ptychography MATLAB package computer software program and associated documentation furnished hereunder (hereafter "PROGRAM").
%
% Terms and Conditions of the LICENSE
% 1.	LICENSOR grants to LICENSEE a royalty-free, non-exclusive license to use the PROGRAM for academic, non-commercial purposes, upon the terms and conditions 
%       hereinafter set out and until termination of this license as set forth below.
% 2.	LICENSEE acknowledges that the PROGRAM is a research tool still in the development stage. The PROGRAM is provided without any related services, improvements 
%       or warranties from LICENSOR and that the LICENSE is entered into in order to enable others to utilize the PROGRAM in their academic activities. It is the 
%       LICENSEE’s responsibility to ensure its proper use and the correctness of the results.”
% 3.	THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR 
%       A PARTICULAR PURPOSE AND NONINFRINGEMENT OF ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. IN NO EVENT SHALL THE LICENSOR, THE AUTHORS OR THE COPYRIGHT 
%       HOLDERS BE LIABLE FOR ANY CLAIM, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE 
%       OF THE PROGRAM OR OTHER DEALINGS IN THE PROGRAM.
% 4.	LICENSEE agrees that it will use the PROGRAM and any modifications, improvements, or derivatives of PROGRAM that LICENSEE may create (collectively, 
%       "IMPROVEMENTS") solely for academic, non-commercial purposes and that any copy of PROGRAM or derivatives thereof shall be distributed only under the same 
%       license as PROGRAM. The terms "academic, non-commercial", as used in this Agreement, mean academic or other scholarly research which (a) is not undertaken for 
%       profit, or (b) is not intended to produce works, services, or data for commercial use, or (c) is neither conducted, nor funded, by a person or an entity engaged 
%       in the commercial use, application or exploitation of works similar to the PROGRAM.
% 5.	LICENSEE agrees that it shall make the following acknowledgement in any publication resulting from the use of the PROGRAM or any translation of the code into 
%       another computing language:
%       "Data processing was carried out using the cSAXS ptychography MATLAB package developed by the Science IT and the coherent X-ray scattering (CXS) groups, Paul 
%       Scherrer Institut, Switzerland."
%
% Additionally, any publication using the package, or any translation of the code into another computing language should cite for difference map:
% P. Thibault, M. Dierolf, A. Menzel, O. Bunk, C. David, F. Pfeiffer, High-resolution scanning X-ray diffraction microscopy, Science 321, 379–382 (2008). 
%   (doi: 10.1126/science.1158573),
% for mixed coherent modes:
% P. Thibault and A. Menzel, Reconstructing state mixtures from diffraction measurements, Nature 494, 68–71 (2013). (doi: 10.1038/nature11806),
% for LSQ-ML method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018
% for OPRP method 
%  M. Odstrcil, P. Baksh, S. A. Boden, R. Card, J. E. Chad, J. G. Frey, W. S. Brocklesby,  "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369
% 6.	Except for the above-mentioned acknowledgment, LICENSEE shall not use the PROGRAM title or the names or logos of LICENSOR, nor any adaptation thereof, nor the 
%       names of any of its employees or laboratories, in any advertising, promotional or sales material without prior written consent obtained from LICENSOR in each case.
% 7.	Ownership of all rights, including copyright in the PROGRAM and in any material associated therewith, shall at all times remain with LICENSOR, and LICENSEE 
%       agrees to preserve same. LICENSEE agrees not to use any portion of the PROGRAM or of any IMPROVEMENTS in any machine-readable form outside the PROGRAM, nor to 
%       make any copies except for its internal use, without prior written consent of LICENSOR. LICENSEE agrees to place the following copyright notice on any such copies: 
%       © All rights reserved. PAUL SCHERRER INSTITUT, Switzerland, Laboratory for Macromolecules and Bioimaging, 2017. 
% 8.	The LICENSE shall not be construed to confer any rights upon LICENSEE by implication or otherwise except as specifically set forth herein.
% 9.	DISCLAIMER: LICENSEE shall be aware that Phase Focus Limited of Sheffield, UK has an international portfolio of patents and pending applications which relate 
%       to ptychography and that the PROGRAM may be capable of being used in circumstances which may fall within the claims of one or more of the Phase Focus patents, 
%       in particular of patent with international application number PCT/GB2005/001464. The LICENSOR explicitly declares not to indemnify the users of the software 
%       in case Phase Focus or any other third party will open a legal action against the LICENSEE due to the use of the program.
% 10.	This Agreement shall be governed by the material laws of Switzerland and any dispute arising out of this Agreement or use of the PROGRAM shall be brought before 
%       the courts of Zürich, Switzerland. 


function [outputs, fourier_error, iter_time] = ptycho_solver(self, par)

import core.*

fprintf('Started solver: %s\n', par.method)

%% initialize GPU wrapper 
par = gpu_wrapper(par);


%% check input values 
[self,par] = initial_checks(self, par);
%% load data to GPU and prepare for reconstruction 
[self,cache] = initialize_solver(self, par);
if par.use_gpu
   [self, cache] =  move_to_gpu(self, cache, par.keep_on_gpu);
end 



fourier_error = Garray( nan(par.Niter, self.Npos, 'single'));
object_norm = Gzeros(par.Nrec,1); 

psi_dash = cell(par.Nmodes, ceil(self.Npos/par.grouping));

aprobe = abs(self.probe{1}(:,:,1,1)); 
cache = precalculate_ROI(self, cache, Ggather(aprobe));

global pprev;
pprev = -1;

t0 = tic;
t_start = tic;
for iter = 1:par.Niter
    
    verbose(1,'Iteration %s: %i / %i  (time %3.3g  avg:%3.3g) \n', par.method, iter, par.Niter, toc(t_start), toc(t0)/(iter-1))
    if par.verbose_level == 0
        progressbar(iter, par.Niter, max(20,round(sqrt(par.Niter))))
    end
    
    t_start = tic;
   
    % update current probe positions (views)
    if iter == 1 ||  iter >= par.probe_pos_search
        % get coordinates of each view (illuminated patch)
        for ll = 1:max(par.Nobjects, par.Nmodes)
            [cache.oROI_s{ll},cache.oROI{ll},sub_px_shift] = find_reconstruction_ROI( self.modes{min(end,ll)}.probe_positions,self.Np_o, self.Np_p); 
            self.modes{min(end,ll)}.sub_px_shift = sub_px_shift; 
        end
    end

    % remove the ambiguity in the probe / object reconstruction => keep average object transmission around 1 
   if  ~is_method(par, {'DM'}) 
        for ll = 1:length(self.object)
            W = cache.illum_sum_0{ll}(cache.object_ROI{:});
            object_norm(ll) = norm2(self.object{ll}(cache.object_ROI{:}).* W ) ./ norm2(W); 
        end
        object_norm = mean(object_norm);
        for ll = 1:par.Nprobes
            self.probe{ll} = self.probe{ll} * object_norm ;  % should avoid abiguity , 
        end
        for ll = 1:length(self.object)
           self.object{ll} = self.object{ll} / object_norm ;  % should avoid abiguity , 
        end
   end
   
    % remove extra degree of freedom  other optimizations
    for ll = 1:par.Nmodes
        for kk = 1:par.Nscans
            ind = self.reconstruct_ind{kk};
            self.modes{ll}.probe_positions(ind,:) = bsxfun(@minus, self.modes{ll}.probe_positions(ind,:), mean(self.modes{ll}.probe_positions(ind,:)) - mean(self.probe_positions_0(ind,:)));
        end
    end
    
    if par.variable_probe && iter > par.probe_reconstruct
        % remove freedom from the OPRP extension 
        vprobe_norm = Ggather(norm2(self.probe{1}(:,:,:,2:end)));
        self.probe{1}(:,:,:,2:end) = self.probe{1}(:,:,:,2:end) ./ vprobe_norm;
        self.probe_evolution(:,2:end) = self.probe_evolution(:,2:end) .* reshape(mean(vprobe_norm,3),1,[]);
        % average variable mode correction is zero 
        self.probe_evolution(ind,:) = self.probe_evolution(ind,:) - mean(self.probe_evolution(ind,:),1); 
        % average intensity correction is zero 
        
        % remove degrees of intensity correction and regularize a bit 
        self.probe_evolution(ind,1) = self.probe_evolution(ind,1)*0.99 + 1; 
        cprobe_norm = norm2(self.probe_evolution(:,1));
        self.probe_evolution(:,1) = self.probe_evolution(:,1) / cprobe_norm;
        self.probe{1}(:,:,:,1) = self.probe{1}(:,:,:,1) .* cprobe_norm;
    end
    
    % updated cached illumination    
    for ll = 1:length(self.object)
        ind = self.reconstruct_ind{ll};
        aprobe2 = abs(self.probe{1}(:,:,min(end,ll),1)).^2; 
        % avoid oscilations by adding momentum term 
        cache.illum_sum_0{ll} = set_projections(cache.illum_sum_0{ll}, Garray(aprobe2), 1,ind, cache)/2;
        cache.illum_norm(ll) = norm2(cache.illum_sum_0{ll});
        cache.MAX_ILLUM(ll) = Ggather(max2(cache.illum_sum_0{ll}));
        cache.weight_proj = sum2(get_projections(cache.illum_sum_0{ll},[],1, ind, cache));
    end
     
    switch  lower(par.method)
        case {'hpie'}
            [self, cache, fourier_error] = engines.hPIE(self,par,cache,fourier_error,iter);
        case {'epie'}
            [self, cache, fourier_error] = engines.ePIE_simple(self,par,cache,fourier_error,iter);
        case {'epie_opr'}
            [self, cache, fourier_error] = engines.ePIE_OPR(self,par,cache,fourier_error,iter);
        case { 'mls','mlc'}
            [self, cache, fourier_error] = engines.LSQML(self,par,cache,fourier_error,iter);
        case 'dm'
            [self, cache,psi_dash,fourier_error] =  engines.DM(self,par,cache,psi_dash,fourier_error,iter);
        otherwise
            error('Not implemented method')
    end

    if any(~isnan(fourier_error(iter,:) ))
        verbose(1,'=====  Fourier error = %3.3g \n ', nanmedian(fourier_error(iter,:)))
    end
    
    
    if par.Nprobes > par.Nrec
        %%  orthogonalization of probe self.modes
         self.probe = ortho_modes(self.probe);
    end


    %% PLOTTING 
    if mod(iter, par.plot_results_every ) == 0 &&  par.plot_results_every ~=0
        if par.variable_probe || par.variable_intensity
            plot_variable_probe(self, par)
        end

       plot_results(self, cache, par,Ggather(self.object{1}), Ggather(self.probe{1}(:,:,1)), Ggather(fourier_error), ...
       self.modes{1}.probe_positions)
        if par.Nprobes > 1 
            plot_modes(self,self.probe,par, 322809, false, 1); 
        end
        if iter >  par.probe_pos_search
            plot_geom_corrections(self, self.modes{1}, self.object{1}, iter, par, cache)
        end
        drawnow
    end
        
end

iter_time = toc(t0) / par.Niter; 
verbose(1,' ====  Time per one iteration %gs \n\n', iter_time)    
outputs = struct();

for ll = 1:par.Nprobes
    outputs.probe{ll} = Ggather(self.probe{ll});
end
for ll = 1:par.Nobjects 
    outputs.object{ll} = Ggather(self.object{ll});
end

fourier_error = Ggather(fourier_error);

outputs.probe_positions = self.modes{1}.probe_positions;

if par.variable_probe
    outputs.probe_evolution = self.probe_evolution;
end


end


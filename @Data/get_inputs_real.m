function self = get_inputs_real(self, data_path,filename, Nx_obj , Ny_obj, ...
    user_scale, lambda, extra_binning)

% changed 'path' variable to 'data_path' so as not to overload the path,
% and added proper path reset after loading data
 import core.* % added to make it compatible with get inputs artificial...
% function
    curr_path = pwd();
    
    data_path = ['data/',data_path]; 
    if iscell(filename)
        fname = filename{1};
    else
        fname = filename; 
    end
    
    disp(['Loading data from ', data_path,'/',fname])
    addpath(data_path)

    [diffraction, mask, noise,probe_positions, probe,  scale, lambda, setup]...
            = prepare_data(data_path, filename,user_scale, lambda, extra_binning);
    rmpath(data_path)

    Npos = size( diffraction, 3);

%    self.user_scale = user_scale; % no longer in class
%    self.rotation = 0; % no longer in class
%     self.scale = scale; % no longer in class
    self.Np_p = size(probe); %  resolution of the difr pattern 
    if Nx_obj == 0 && Ny_obj == 0
        self.Np_o = get_object_extent(size(probe), probe_positions, 0.1);
        self.Np_o = round(self.Np_o.*1.05);
    else
        self.Np_o = [ Nx_obj , Ny_obj]; %  resolution of the difr pattern 
    end
    
%     keyboard
    
    self.Npos = Npos; 
    self.probe{1} = probe;
    self.probe_positions_0 = probe_positions; % nan(Npos,2);
    self.probe_positions = probe_positions;
%     self.pos_error = 0; % 0.05*sqrt(sum(abs(probe(:)))/pi);
%     self.pos_regularization = 5; % regularize position search 

%    self.mask = mask;
%    self.noise  = noise;
    self.diffraction = diffraction;
    self.object{1} =   ones(self.Np_o, 'single') ... 
        .* exp(2i*pi*rand(self.Np_o) * 1e-4  ) ;  % not so much random 
%    self.filename = filename;
%    self.path = data_path;
    self.lambda = lambda;
   % self.diff_pattern_blur = 0;
%    self.setup = setup;
    
    if isfield(setup, 'reconstruct_ind')
        self.reconstruct_ind =  setup.reconstruct_ind;
    else
        self.reconstruct_ind{1} = 1:self.Npos;
    end
    
%     keyboard
% need to set px_scale before we leave this - scale * user_scale?? or not?
    self.px_scale = scale * user_scale;
    
    
end
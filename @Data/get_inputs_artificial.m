% simple rutine to get artificial dataset 

% Academic License Agreement
%
% Source Code
%
% Introduction 
% •	This license agreement sets forth the terms and conditions under which the PAUL SCHERRER INSTITUT (PSI), CH-5232 Villigen-PSI, Switzerland (hereafter "LICENSOR") 
%   will grant you (hereafter "LICENSEE") a royalty-free, non-exclusive license for academic, non-commercial purposes only (hereafter "LICENSE") to use the cSAXS 
%   ptychography MATLAB package computer software program and associated documentation furnished hereunder (hereafter "PROGRAM").
%
% Terms and Conditions of the LICENSE
% 1.	LICENSOR grants to LICENSEE a royalty-free, non-exclusive license to use the PROGRAM for academic, non-commercial purposes, upon the terms and conditions 
%       hereinafter set out and until termination of this license as set forth below.
% 2.	LICENSEE acknowledges that the PROGRAM is a research tool still in the development stage. The PROGRAM is provided without any related services, improvements 
%       or warranties from LICENSOR and that the LICENSE is entered into in order to enable others to utilize the PROGRAM in their academic activities. It is the 
%       LICENSEE’s responsibility to ensure its proper use and the correctness of the results.”
% 3.	THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR 
%       A PARTICULAR PURPOSE AND NONINFRINGEMENT OF ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. IN NO EVENT SHALL THE LICENSOR, THE AUTHORS OR THE COPYRIGHT 
%       HOLDERS BE LIABLE FOR ANY CLAIM, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE 
%       OF THE PROGRAM OR OTHER DEALINGS IN THE PROGRAM.
% 4.	LICENSEE agrees that it will use the PROGRAM and any modifications, improvements, or derivatives of PROGRAM that LICENSEE may create (collectively, 
%       "IMPROVEMENTS") solely for academic, non-commercial purposes and that any copy of PROGRAM or derivatives thereof shall be distributed only under the same 
%       license as PROGRAM. The terms "academic, non-commercial", as used in this Agreement, mean academic or other scholarly research which (a) is not undertaken for 
%       profit, or (b) is not intended to produce works, services, or data for commercial use, or (c) is neither conducted, nor funded, by a person or an entity engaged 
%       in the commercial use, application or exploitation of works similar to the PROGRAM.
% 5.	LICENSEE agrees that it shall make the following acknowledgement in any publication resulting from the use of the PROGRAM or any translation of the code into 
%       another computing language:
%       "Data processing was carried out using the cSAXS ptychography MATLAB package developed by the Science IT and the coherent X-ray scattering (CXS) groups, Paul 
%       Scherrer Institut, Switzerland."
%
% Additionally, any publication using the package, or any translation of the code into another computing language should cite for difference map:
% P. Thibault, M. Dierolf, A. Menzel, O. Bunk, C. David, F. Pfeiffer, High-resolution scanning X-ray diffraction microscopy, Science 321, 379–382 (2008). 
%   (doi: 10.1126/science.1158573),
% for mixed coherent modes:
% P. Thibault and A. Menzel, Reconstructing state mixtures from diffraction measurements, Nature 494, 68–71 (2013). (doi: 10.1038/nature11806),
% for LSQ-ML method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018
% for OPRP method 
%  M. Odstrcil, P. Baksh, S. A. Boden, R. Card, J. E. Chad, J. G. Frey, W. S. Brocklesby,  "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369
% 6.	Except for the above-mentioned acknowledgment, LICENSEE shall not use the PROGRAM title or the names or logos of LICENSOR, nor any adaptation thereof, nor the 
%       names of any of its employees or laboratories, in any advertising, promotional or sales material without prior written consent obtained from LICENSOR in each case.
% 7.	Ownership of all rights, including copyright in the PROGRAM and in any material associated therewith, shall at all times remain with LICENSOR, and LICENSEE 
%       agrees to preserve same. LICENSEE agrees not to use any portion of the PROGRAM or of any IMPROVEMENTS in any machine-readable form outside the PROGRAM, nor to 
%       make any copies except for its internal use, without prior written consent of LICENSOR. LICENSEE agrees to place the following copyright notice on any such copies: 
%       © All rights reserved. PAUL SCHERRER INSTITUT, Switzerland, Laboratory for Macromolecules and Bioimaging, 2017. 
% 8.	The LICENSE shall not be construed to confer any rights upon LICENSEE by implication or otherwise except as specifically set forth herein.
% 9.	DISCLAIMER: LICENSEE shall be aware that Phase Focus Limited of Sheffield, UK has an international portfolio of patents and pending applications which relate 
%       to ptychography and that the PROGRAM may be capable of being used in circumstances which may fall within the claims of one or more of the Phase Focus patents, 
%       in particular of patent with international application number PCT/GB2005/001464. The LICENSOR explicitly declares not to indemnify the users of the software 
%       in case Phase Focus or any other third party will open a legal action against the LICENSEE due to the use of the program.
% 10.	This Agreement shall be governed by the material laws of Switzerland and any dispute arising out of this Agreement or use of the PROGRAM shall be brought before 
%       the courts of Zürich, Switzerland. 

function self = get_inputs_artificial(self, scan)
% Description: simple rutine to get artificial dataset 


    import core.*

    %%%% load some artificial object %%%%%%%%%%%%%%%%%%%%%%%%%%
    object_0 = [];
    for i = 1:length(scan.dataset_id)
        [object_0(:,:,i), probe_0] = load_data(scan.dataset_id(i));
    end

    object_0 = object_0 / max(abs(object_0(:)));
    object_0 = (scan.object_contrast*object_0 + (1-scan.object_contrast)) ;

    %%% make a scanning path %%%%%%%%%%%%%%%


    step = mean(scan.Np_p * (1-scan.overlap) / 2 );
    switch scan.pattern
        case 'circular'
            probe_positions = make_circular_scan([-scan.N_pts_x,scan.N_pts_x,-scan.N_pts_y,scan.N_pts_y]*step/2, step, 0, true);
        case 'regular'
            probe_positions = make_reg_scan(step,scan.N_pts_x,scan.N_pts_y, 0.1);
        case 'fermat'
            probe_positions = make_fermat_scan(scan.N_pts_x,scan.N_pts_y,step);
    end

    probe_positions = round(probe_positions);

    %%%%%%%%%%%%%  prepare a scanning probe %%%%%%%% 

    fprintf('Nscaning positions %i \n ', length(probe_positions))
    Npos = length(probe_positions);

    
    if isempty(probe_0)
        load utils/imgs/probe_PSI.mat
        fprobe = fftshift(fft2(fftshift(probe)));
        fprobe = crop_pad(fprobe, [160, 168]);  % remove artefacts 
        % upscale to the requested resolution 
        if any(scan.Np_p < size(fprobe)); error('Requested probe would be undesampled'); end
        probe = ifftshift(ifft2(fftshift(crop_pad(fprobe, scan.Np_p))));
        [x,y] = center(abs(probe));
        probe = imshift_fft(probe, -x,-y);        
    else
        scan.Np_p = size(probe_0);
        probe = probe_0;
    end
    Np_o = get_object_extent(scan.Np_p, probe_positions, 0.1);
    
    % just use function imshift_fast to embed the object in array of size Np_o
    object_0 =  crop_pad(object_0, Np_o);

    
    %%%%% prepare "measured" data  
    [diffraction, ~,object_0] ...
        = create_data(object_0, probe, probe_positions, scan ) ;
    
    
    %%%%%  set everything to the self object 
    
    self.Np_p = scan.Np_p; %  resolution of the difr pattern 
    self.Np_o = Np_o; %  resolution of the difr pattern 

    self.Npos = Npos; 
    self.probe{1} = probe;
    self.probe_positions_0 = probe_positions; 
    self.probe_positions = [];    
 
    self.reconstruct_ind{1} = 1:self.Npos;

    self.diffraction = diffraction;
    self.object{1} =  single(  (rand(self.Np_o))  ...
        .* exp(2i*pi*rand(self.Np_o) * 1e-4  ) );  % not so much random 
    self.object_orig = object_0;
    self.probe_orig = probe;
    
end

    

function [object, probe] = load_data(id)
    %% preload some artificial object 
    
    probe = [];

    switch id
        case 1
            %% MONA LISA
         object = imread('utils/imgs/ML512.jpg');
         object = double(object(:,:,1));
         object = object./max(object(:)); % normalise
         object = object .* exp(2i*pi*object);
        case 2
            %% MANDRIL 
            object = imread('utils/imgs/mandrill.png');
            object = single(object) / 255; 
            object = object .* exp(2i*pi*object);
        case 3
            %% USAF target 
            object = single(255-imread('utils/imgs/USAF-1951.jpg'));
            object = tile_2n(object);
            object = exp(1i*object / max(object(:)));
        otherwise
            error('Missing dataset')
    end
    
    % replicate object along both axis 
    object = repmat(object, 3,3);
     
    object(object==0) = 1;
    object = single(object);
end

function [ diffraction, noise_pattern, object]  = ...
    create_data(object, probe, probe_positions,  scan ) 

    %% create an diffraction data 
    [Np_o(1), Np_o(2), ~]=size(object);
    [Np_p(1), Np_p(2), ~] = size(probe);


    object = object / mean(abs(object(:)));

    Npos = size(probe_positions,1);
    [~,oROI,sub_px_shift]  = find_reconstruction_ROI( probe_positions,Np_o, Np_p);

    illum_proj = zeros(Np_p(1),Np_p(2),Npos, 'single');
    for ii = 1:Npos
        P = imshift_fft(probe(:,:,1),sub_px_shift(ii,1),sub_px_shift(ii,2));
        O = object(oROI{ii,:},1);
        illum_proj(:,:,ii) = P.*O ; % ones(Np_p); %  object(oROI{ii,:}, 1) .* probe;
    end

    %%  far field 
    diffraction = fftshift_2D(fft2(fftshift_2D(illum_proj)));  
    
    % add simple errors in "wavefront"
    %diffraction= imshift_fft(diffraction, 0, 0.5*randn(Npos,1));
    
    diffraction = abs(diffraction).^2;
    

    if ~isinf(scan.N_photons)
        diffraction = diffraction / mean(sum2(diffraction)) * scan.N_photons ; %  get more flux in case of lower coherence
        disp(['Max photons ', num2str(ceil(max(diffraction(:))))])
        disp('Adding Poisson noise')
        ratio = 1e-6;
        diffraction = imnoise(diffraction * ratio, 'poisson')/ratio;
    end

    noise_pattern = [];
    
    if ~isinf(scan.N_photons)
        diffraction= round(diffraction);
    end
end

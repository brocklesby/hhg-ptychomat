clear all
%function [outputs, FEsum] = ptychography_artificial(user_scale)
%if nargin==0
     user_scale = 0.28
%end
addpath('utils')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%% SETTINGS OF THE GENERATED ARTIFICIAL DATASET  %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
scan.overlap = 0.7; % desired scan.overlap as a function of diameter !! optimal value > 0.7 !!
scan.N_pts_x = 15; % number of horizontal scan positions
scan.N_pts_y = 15; % number of vertical scan positions
scan.Np_p = [256, 256];   % probe size in pixels 
scan.N_photons = 4e5;      % number of photons per diffraction pattern, inf == no noise 
scan.pattern = 'fermat';   % scanning pattern - circular, regular, fermat 
scan.dataset_id = 2;      % number of artificial dataset - Mona Lisa, Mandrill, USAF 
scan.object_contrast = 0.2;  % reduce simulated object contrast 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% GENERATE ARTIFICIAL DATASET %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%inputs = Data('artificial', scan);
% inputs.show_data

%%%%%%%%%%%%%%% USE REAL DATASET %%%%%%%%%%%%%%%%%
% try a real dataset
%inputs = Data('real', 'visible_light','20170317/ptycho_16-58-11', 0, 0,  0.594, 633e-9 , 2);
%inputs = Data('real', 'visible_light','20170518/ptycho_13-15-50', 0, 0,  1.15, 633e-9 , 2);
%inputs = Data('real', 'xray','20171123/ptycho_22-12-46', 0, 0,  user_scale, 29.4e-9 , 1);
%inputs = Data('real', 'xray','20170426/ptycho_01-15-39', 0, 0,  user_scale, 29.4e-9 , 1);
%inputs = Data('real', 'xray','20180118/ptycho_17-08-29', 0, 0,  user_scale, 29.4e-9 , 1);
inputs = Data('real', 'xray','20180119/ptycho_13-22-24', 0, 0,  user_scale, 29.4e-9 , 1);
%inputs = Data('real', 'xray','20180119/ptycho_21-04-54', 0, 0,  user_scale, 29.4e-9 , 2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% DEFAULT SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

param.use_gpu = true; 
param.keep_on_gpu = true; 
param.gpu_id = []; % used GPU id, [] means chosen by matlab

%% basic recontruction parameters 
% choose solver 
param.method = 'MLs';     % choose reconstruction method: ePIE,ePIE_OPR, DM, MLs, MLc 

%% PIE / ML 
% betas are step sizes, faster convergence vs higher stability
param.beta_object = 1;
param.beta_probe = 1;    
param.beta_LSQ = true;   % use LSQ preditive step for ML method 
param.delta_p = 0.1;     % LSQ damping constant , used only for MLs method

%% DM
param.pfft_relaxation = 0.05;  % relaxed modulus constraint for DM solver 
param.probe_inertia = 0.1; % add inertia to the probe reconstruction to avoid oscilations 
%% general 
param.Nprobes = 1; % mumber of probes 
param.probe_reconstruct = 1;  % iteration when the probe reconstruction is started
param.object_reconstruct = 1;% iteration when the object reconstruction is started
param.Niter = 300 ;       % number of iterations for the chosen engine 
param.grouping = 100;     % size of the processed blocks 
param.verbose_level = 0;  % verbosity of the solver , 0 = quite, 4 = all 
param.plot_results_every = 50;
param.probe_support_radius = [];  % radius of circular probe suppport, [] = none, useful for DM code 
param.probe_support_distance = 0; % distance of the probe support from the object plane, 0 = none, inf = farfield

%% ADVANCED OPTIONS       
param.variable_probe = false;           % Use SVD to account for variable illumination during a single scan (Orthogonal probe relaxation)
param.variable_probe_modes = 3;         % Number of SVD modes used for the orthogonal probe relaxation, ML method allows only one variable mode ! 
param. variable_intensity = false;      % Account for variable illumination intensity during single scan

param.apply_subpix_shift = false;       % apply FFT-based subpixel shift, important for good position refinement but it is slow
param.probe_pos_search = inf;            % iteration number from which position correction is started 

param.apply_multimodal_update = false; % apply higher Thibault's modes to the object update, may result in lower object quality for real datasets

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% add position error
% inputs.probe_positions_0(:,1) = inputs.probe_positions_0(:,1)+randn(inputs.Npos,1);
    
%% initial reconstruction
param.Niter =50;
param.Nprobes = 1;
param.probe_reconstruct = 20;  % iteration when the probe reconstruction is started
param.plot_results_every = 10;
param.probe_support_radius = 0.7;
param.probe_support_distance = 0;  % realspace support 
param.method = 'DM';  %% ePIE, ePIE_OPR,DM, MLs (ePIE like partially parallel method), MLc (ML like partially parallel code)

% run solver, DM is a good initial guess 
[outputs, fourier_error] = core.ptycho_solver(inputs, param);
 

% return reconstructed values as an initial guess
inputs = inputs.update(outputs);



%% refinement reconstruction 
do_second = 1;
if do_second ==1
param.probe_reconstruct = 20;  %needed because of error in hPIE =can't delay the probe variation
param. apply_subpix_shift = true;       % apply FFT-based subpixel shift, important for good position refinement but it is slow
param.probe_pos_search = true;          % iteration number from which position correction is started 
param. variable_probe = true;           % Use SVD to account for variable illumination during single scan
param. variable_intensity = true;           % Account for variable illumination intensity during single scan

% apply farfield support, can be useful constraint in for zoneplate focused beam 
%param.probe_support_radius = 0.7;  % support radius
%param.probe_support_distance = inf;  % farfield support 
param.probe_support_distance = 0;  % support is near the object - actually abotu 100 um?

param.verbose_level = 0; %param. apply_subpix_shift = true;  
%param.method = 'hPIE';   %% ePIE,ePIE_OPR, DM, MLs (ePIE like partially parallel method), MLc (ML like partially parallel code)%
param.method = 'MLc';   %% ePIE,ePIE_OPR, DM, MLs (ePIE like partially parallel method), MLc (ML like partially parallel code)
param.delta_p = 0.1;     % LSQ damping constant, improves convergence for the MLc method
param.grouping = 50;  % size of the parallel groups 
param.Niter = 1000;
[outputs, fourier_error] = core.ptycho_solver(inputs, param);

% inputs = inputs.update(outputs);
%FEsum = sum(sum(fourier_error(~isnan(fourier_error))))

%FEsum = sum(fourier_error(end,:))
figure; imagesc_hsv(outputs.object{1}); axis equal tight

end

%% get last fourier error - need to get non-NaN ones
error_sum = sum(fourier_error,2);
error_exists =~isnan(error_sum);  % these are the ones that exist
error_sum = error_sum(error_exists); % remove NaN values
FEsum = error_sum(end); % ONLY THE LAST ONE!!!

%end % function version


% Academic License Agreement
%
% Source Code
%
% Introduction 
% •	This license agreement sets forth the terms and conditions under which the PAUL SCHERRER INSTITUT (PSI), CH-5232 Villigen-PSI, Switzerland (hereafter "LICENSOR") 
%   will grant you (hereafter "LICENSEE") a royalty-free, non-exclusive license for academic, non-commercial purposes only (hereafter "LICENSE") to use the cSAXS 
%   ptychography MATLAB package computer software program and associated documentation furnished hereunder (hereafter "PROGRAM").
%
% Terms and Conditions of the LICENSE
% 1.	LICENSOR grants to LICENSEE a royalty-free, non-exclusive license to use the PROGRAM for academic, non-commercial purposes, upon the terms and conditions 
%       hereinafter set out and until termination of this license as set forth below.
% 2.	LICENSEE acknowledges that the PROGRAM is a research tool still in the development stage. The PROGRAM is provided without any related services, improvements 
%       or warranties from LICENSOR and that the LICENSE is entered into in order to enable others to utilize the PROGRAM in their academic activities. It is the 
%       LICENSEE’s responsibility to ensure its proper use and the correctness of the results.�?
% 3.	THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR 
%       A PARTICULAR PURPOSE AND NONINFRINGEMENT OF ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. IN NO EVENT SHALL THE LICENSOR, THE AUTHORS OR THE COPYRIGHT 
%       HOLDERS BE LIABLE FOR ANY CLAIM, DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE 
%       OF THE PROGRAM OR OTHER DEALINGS IN THE PROGRAM.
% 4.	LICENSEE agrees that it will use the PROGRAM and any modifications, improvements, or derivatives of PROGRAM that LICENSEE may create (collectively, 
%       "IMPROVEMENTS") solely for academic, non-commercial purposes and that any copy of PROGRAM or derivatives thereof shall be distributed only under the same 
%       license as PROGRAM. The terms "academic, non-commercial", as used in this Agreement, mean academic or other scholarly research which (a) is not undertaken for 
%       profit, or (b) is not intended to produce works, services, or data for commercial use, or (c) is neither conducted, nor funded, by a person or an entity engaged 
%       in the commercial use, application or exploitation of works similar to the PROGRAM.
% 5.	LICENSEE agrees that it shall make the following acknowledgement in any publication resulting from the use of the PROGRAM or any translation of the code into 
%       another computing language:
%       "Data processing was carried out using the cSAXS ptychography MATLAB package developed by the Science IT and the coherent X-ray scattering (CXS) groups, Paul 
%       Scherrer Institut, Switzerland."
%
% Additionally, any publication using the package, or any translation of the code into another computing language should cite for difference map:
% P. Thibault, M. Dierolf, A. Menzel, O. Bunk, C. David, F. Pfeiffer, High-resolution scanning X-ray diffraction microscopy, Science 321, 379–382 (2008). 
%   (doi: 10.1126/science.1158573),
% for mixed coherent modes:
% P. Thibault and A. Menzel, Reconstructing state mixtures from diffraction measurements, Nature 494, 68–71 (2013). (doi: 10.1038/nature11806),
% for LSQ-ML method 
% M. Odstrcil, A. Menzel, M.G. Sicairos,  Iterative least-squares solver for generalized maximum-likelihood ptychography, Optics Express, 2018
% for OPRP method 
%  M. Odstrcil, P. Baksh, S. A. Boden, R. Card, J. E. Chad, J. G. Frey, W. S. Brocklesby,  "Ptychographic coherent diffractive imaging with orthogonal probe relaxation." Optics express 24.8 (2016): 8360-8369
% 6.	Except for the above-mentioned acknowledgment, LICENSEE shall not use the PROGRAM title or the names or logos of LICENSOR, nor any adaptation thereof, nor the 
%       names of any of its employees or laboratories, in any advertising, promotional or sales material without prior written consent obtained from LICENSOR in each case.
% 7.	Ownership of all rights, including copyright in the PROGRAM and in any material associated therewith, shall at all times remain with LICENSOR, and LICENSEE 
%       agrees to preserve same. LICENSEE agrees not to use any portion of the PROGRAM or of any IMPROVEMENTS in any machine-readable form outside the PROGRAM, nor to 
%       make any copies except for its internal use, without prior written consent of LICENSOR. LICENSEE agrees to place the following copyright notice on any such copies: 
%       © All rights reserved. PAUL SCHERRER INSTITUT, Switzerland, Laboratory for Macromolecules and Bioimaging, 2017. 
% 8.	The LICENSE shall not be construed to confer any rights upon LICENSEE by implication or otherwise except as specifically set forth herein.
% 9.	DISCLAIMER: LICENSEE shall be aware that Phase Focus Limited of Sheffield, UK has an international portfolio of patents and pending applications which relate 
%       to ptychography and that the PROGRAM may be capable of being used in circumstances which may fall within the claims of one or more of the Phase Focus patents, 
%       in particular of patent with international application number PCT/GB2005/001464. The LICENSOR explicitly declares not to indemnify the users of the software 
%       in case Phase Focus or any other third party will open a legal action against the LICENSEE due to the use of the program.
% 10.	This Agreement shall be governed by the material laws of Switzerland and any dispute arising out of this Agreement or use of the PROGRAM shall be brought before 
%       the courts of Zürich, Switzerland. 

